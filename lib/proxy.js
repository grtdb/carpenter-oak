const ngrok = require('ngrok');

(async function() {
	const url = await ngrok.connect( {
		host_header: 'co.local'
	} );

	console.log(url);
})();