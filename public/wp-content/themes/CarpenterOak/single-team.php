<?php /* Standard Page Template */
get_header(); the_post(); $page_id = get_the_ID(); get_hero('team'); ?>

<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "HomeAndConstructionBusiness",
	  "name": "Carpenter Oak Employee",
	  "employee": {
	    "@type": "<?= get_field('team_job_title'); ?>",
        "employee": {
	        "@type": "Person",
	        "name": "<?php the_title(); ?>"
	    }
	  }
	}
</script>

<section id="team-profile">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-lg-3 sub-menu side-bar">
				<ul class="sub_menu">
					<li>
						<?php $back_url = htmlspecialchars($_SERVER['HTTP_REFERER']); ?>
						<a href="<?= $back_url; ?>" class="append-arrow-left">Back</a>
					</li>
				</ul>
			</div>
			<div class="col-md-12 col-lg-9 sub-content">
				<h2><?php the_title(); ?></h2>

				<div class="post_nav">
					<?php
					$prev_post = get_next_post();
					if ($prev_post) :
						$prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
						echo "\t" . '<a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class="prev_post">'. $prev_title . '</a>' . "\n";
					endif;

					$next_post = get_previous_post();
					if ($next_post) :
						$next_title = strip_tags(str_replace('"', '', $next_post->post_title));
						echo "\t" . '<a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class="next_post">'. $next_title . '</strong></a>' . "\n";
					endif;
					?>
				</div>
				<br>
				<h3><span class="text-background"><?= get_field('team_job_title'); ?></span></h3>


				<div class="row">
					<div class="col-md-7 col-lg-6">
						<?php the_content(); ?>
					</div>
					<div class="col-md-5 col-lg-6">
						<?php
						if (has_post_thumbnail( $page_id ) ) {
							?>
							<div class="profile-image" style="background-image: url(<?php echo get_the_post_thumbnail_url($page_id,'full');  ?>);"></div>
							<?php
						} ?>

						<?php
						$contactNumber   = get_field('team_contact_number');
						$contactEmail    = get_field('team_email');
						$contactLinkedin = get_field('team_linkedin_profile');

						if ( $contactNumber || $contactEmail || $contactLinkedin) {
							?>
							<h4 class="text-orange">Contact <?php the_title(); ?></h4>
							<?php
						}
						?>

						<?php
						if ($contactNumber) :
							?>
							<p><span class="icon icon--phone"><?= $contactNumber; ?></span></p>
							<?php
						endif;

						if ($contactEmail) :
							?>
							<p><span class="icon icon--email"><a href="mailto:<?= $contactEmail; ?>"><?= $contactEmail; ?></a></span></p>
							<?php
						endif;

						if ($contactLinkedin) :
							?>
							<p><span class="icon icon--linkedin"><a href="<?= $contactLinkedin; ?>">Connect on LinkedIn</a></span></p>
							<?php
						endif;
						?>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer();