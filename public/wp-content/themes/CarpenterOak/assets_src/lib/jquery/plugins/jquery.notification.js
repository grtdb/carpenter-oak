/*!
	jQuery Notification v1.4.1
	(c) 2015 Andy Palmer
	license: http://www.opensource.org/licenses/mit-license.php
*/
'use strict';
;(function($) {
	$.fn.notification = function() {

		var options, messages, self, no_input = [], input, error;

		if(arguments.length === 1 && typeof arguments[0] === 'object') {
			options = arguments[0];
		} else {
			if(typeof arguments[1] === 'string') {
				messages = [arguments[1]];
			} else {
				messages = arguments[1];
			}

			options = {
				type: arguments[0],
				messages: messages
			};
		}

		var defaults = {
			messages: [],
			scroll: true,
			type: 'success',
			errorPosition: 'inline',
			showBox: true,
		}, settings, html;

		if(options.type === 'error') {
			defaults.showBox = false;
		} else if(options.type === 'success') {
			defaults.showBox = true;
		}

		if(typeof options === 'object') {
			settings = $.extend({}, defaults, options);
		} else if(typeof options === 'string') {
			settings = $.extend({}, defaults, {messages: [options]});
		}

		$('#notification,.form-errors,.form-error').remove();

		if(!$.isEmptyObject(settings.messages)) {

			if(settings.showBox) {
				html = '<div id="notification" class="' + settings.type + '">';

				$.each(settings.messages, function(key, value) {
					html += '<p>' + value + '</p>';
				});

				html += '</div>';

				$('#contact-form .contact-top').append(html);

				var gap = 10;
				gap += $( '#masthead' ).outerHeight();
				if ( $( '#wpadminbar' ).length ) {
					gap += $( '#wpadminbar' ).outerHeight();
				}
				if (isMobile()) {
					var view_height = $(window).height();
					var notif_height = $('#notification').outerHeight();
					gap = (view_height - notif_height) / 2;
				}
				if(settings.scroll || isMobile()) {
					$('html,body').animate({scrollTop: $('#notification').offset().top - gap});
				}

			} else {

				self = this;

				self.find(':input').removeClass('input-error').next('.form-error').remove();

				$.each(settings.messages, function(key, value) {

					input = self.find(':input[name=' + key + ']');

					if(!input.length) {
						// If the input wasn't found, try to find one where the name starts with key in case it's an array i.e shipping_method[0]
						input = self.find(':input[name^=' + key + ']');
					}

					if(input.length) {

						input.addClass('input-error').one('focus', function() {

							$(this).removeClass('input-error');
							self.find('.form-error[data-key=' + key + ']').remove();
							self.next('.form-error[data-key=' + key + ']').remove();

						});

						self.find('.form-error[data-key=' + key + ']').remove();

						error = '<div class="form-error" data-key="' + key + '">' + value + '</div>';

						var errorPosition = input.data('error_position') || settings.errorPosition;

						switch(errorPosition) {
							case 'top':
								self.prepend(error);
							break;
							case 'bottom':
								self.append(error);
							break;
							case 'afterParent':
								input.parent().after(error);
							break;
							default:
								input.after(error);
							break;
						}


					} else {
						no_input.push(value);
					}
				});

				// If showBox is false and there are errors without a corresponding input field we need to display them above the form
				if(no_input.length) {
					self.find('.form-errors').remove();

					self.prepend('<div class="form-errors"><p class="form-error">' + no_input.join('</p><p class="form-error">') + '</p></div>');
				}
			}
		}

		return this;
	};
})(jQuery);