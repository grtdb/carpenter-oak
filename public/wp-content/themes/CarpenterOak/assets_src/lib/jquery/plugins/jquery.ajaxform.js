/*!
	jquery-ajaxform v1.4.2
	A jQuery AJAX form submission plugin used by ExleySmith
	(c) 2015 Andy Palmer
	license: http://www.opensource.org/licenses/mit-license.php
*/
(function($) {
	'use strict';

	$.fn.ajaxForm = function(options) {

		var settings = $.extend({}, $.fn.ajaxForm.defaults, options);

		return this.each(function() {

			$(this).on('submit', function(e) {

				e.preventDefault();

				$('html').addClass('wait');

				$(this).find(':input[type=submit]').prop('disabled', true);

				var action = $(this).attr('action') || window.ajaxurl || '/';

				$.ajax({
					url: action,
					type: 'POST',
					dataType: 'json',
					data: $(this).serialize(),
					context: this,
					success: function(json) {

						if( json.fragments ) {

							$.each(json.fragments, function(key, value) {
								$(key).replaceWith(value);
							});
						}

						var notification_opts;
						if( json.errors ) {

							notification_opts = {
								type: 'error',
								messages: json.errors
							};

							if( typeof $(this).data('options') === 'object' ) {
								notification_opts = $.extend(notification_opts, $(this).data('options'));
							}

							$(this).notification(notification_opts);

							if( $.isFunction(settings.failure) ) {
								settings.failure.call(this, json.errors);
							}

						} else if( json.success ) {

							notification_opts = {
								type: 'success',
								messages: [ json.success ]
							};

							if(typeof $(this).data('options') === 'object') {
								notification_opts = $.extend(notification_opts, $(this).data('options'));
							}

							$(this).notification(notification_opts);

							$(this).find(':checkbox').prop('checked', false);
							$(this).find(':input').not('[type=submit],[type=hidden]').val('');

							if( $.isFunction(settings.success) ) {
								settings.success.call(this, json.success);
							}

							if(json.redirect) {
								window.location = json.redirect;
								return;
							} else if(json.reload) {
								window.location.reload();
							}
						}
					},
					complete: function() {
						$(this).find(':input[type=submit]').prop('disabled', false);
						$('html').removeClass('wait');
					}
				});
			});
		});
	};

	$.fn.ajaxForm.defaults = {

	};

	$(function() {
		$('form.ajax-form[data-ajax-auto-init!=false]').ajaxForm();
	});

})(jQuery);
