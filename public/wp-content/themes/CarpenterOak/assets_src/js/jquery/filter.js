jQuery(function($) {
	$( document ).ready( function() {
		$('.ajax-item').addClass('in');

		// ajax filtering
		$('.ajax-filter').click( function (e) {
			if ($(this).data('template') == 'news/grid' ) {
				e.preventDefault();
			}
			filter( $(this), true );
		} );

		$('.filters__apply-filters').click( function() {
			var $source = $('.ajax-filter.active');
			filter( $source, true  );
		} );

		$('.filters input').change( function() {
			$('.filters__apply-filters').css('display', 'inline-block');
			$('.filters__clear-filters').css('display', 'inline-block');
		} );

		$('#filters').on('show.bs.collapse', function () {
			if ( $('.filters input').is(':checked')) {
				$('.filters__clear-filters').css('display', 'inline-block');
			}
		} );

		$('.filters__clear-filters').click(function () {
			$('.filters input').prop('checked', false);
			var $source = $('.ajax-filter.active');
			filter( $source , false );
		});
		// If filters are already checked, run `filter`
		if ( $('#filters input:checked').length ) {

			filter( $('.ajax-filter.active'), false);
		}
	} );

	function filter( $source, $collapse ) {

		if ( $collapse == true ) {
			$('#filters').collapse('hide');
		}

		var filter  = $source;
		var filters = $('#sub-menu form.menuFilters, #filters form.filters').serialize();

		var data = {
			'action': 'filter',
			'term_id': $source.data('termid'),
			'post_type': $source.data('posttype'),
			'template': $source.data('template'),
			'filters': filters
		};



		$.ajax({
			url: filter_params.ajaxurl,
			data: data,
			type: 'POST',
			dataType: 'json',
			beforeSend: function (xhr) {
				$('.active').removeClass('active');
				filter.addClass('active');
				filter.next('label').addClass('active');
				$('.ajax-item').each(function () {
					var height = $(this).outerHeight();
					$(this).height(height);
				});
				$('.ajax-item').removeClass('in');
				$('.ajax-item').addClass('active');

			},
			success: function (data) {
				$('.loadmore').removeClass('d-none');
				if ( data.posts ) {
					var ajaxData = data.posts;
					if ( ! $('.news').length ) {
						$('.archive--intro__content h1.page-title').html( data.title );
						$('.archive-description').html(data.content);
					}


					if( !$.trim( $(ajaxData).html() ).length ) {
						$('.loadmore').addClass('d-none');
						$('.ajax-wrapper').html( '<p style="padding:50px 0;margin:0 auto;">Sorry there are no projects with these filters</p> ');
					} else {
						$('.ajax-wrapper').html( ajaxData );
					}
					setTimeout(function () {
						$('.ajax-item').addClass('in');
						lazy();
					}, 200);
					window.history.pushState("Details", "Title",
						filter.attr('href')
					);

					// Reset loadmore page
					if ( loadmore_params !== undefined ) {
						loadmore_params.posts        = data.query; //because data.posts is the actual posts here
						loadmore_params.current_page = data.current_page;
						loadmore_params.max_page     = data.max_page;

						if ((data.current_page >= data.max_page) ) {

							$('.loadmore').addClass('d-none');

						} else {

							$('.loadmore').removeClass('d-none');

						}
					}
					if (filter.data('category') === '') {
						window.history.pushState( "Details", "Title", location.pathname );
					} else {
						window.history.pushState( "Details", "Title", filter.data('category')  );
					
						if ( filter.data('category') && filter.data('category').startsWith('?project-category')) {
							var catgegory = filter.data('category').split('=')[1];
							var url = 'https://' + location.hostname + '/project-category/' + catgegory + '/';
			
							var canonicallink = document.createElement('link');
							canonicallink.id = 'canonical-project';
							canonicallink.rel = 'canonical';
							canonicallink.href = url;

							if ( $('#canonical-project') ) {
								$('#canonical-project').remove();
								document.head.prepend(canonicallink);
							}

							
							

						}
					}
				} else {
					$('.ajax-wrapper').html( data.error );
				}
			}
		});
	}
} );