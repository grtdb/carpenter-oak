$( document ).ready( function() {
	projectStart_bindForm();
} );

function projectStart_bindForm() {

	// Bind progress except for last item
	$('#project-start__carousel .carousel-item:not(:last-child) .project-start__set .project-start__option a').click( function( e ) {
		e.preventDefault();
		projectStart_progress()
	} );

	// Always bind change value
	$('.project-start__set .project-start__option a').click( function( e ) {
		e.preventDefault();
		var $set = $(this).closest('.project-start__set');
		projectStart_updateValue( $set, $(this).data('value') );
	} );

	// Bind form submit on last item
	$('#project-start__carousel .carousel-item:last-child .project-start__set .project-start__option a').click( function( e ) {
		e.preventDefault();
		$(this).closest('form').submit();
	} );
}

function projectStart_updateValue( $set, value ) {
	$set.find('input').val( value );
}

function projectStart_progress() {
	var $carousel = $('#project-start__carousel');
	$carousel.carousel('next');
}