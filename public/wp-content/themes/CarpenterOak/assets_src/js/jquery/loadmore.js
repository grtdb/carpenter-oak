// loadmore_params injected from wp_localize_script()

jQuery(function($){
	$('.loadmore').click(function(){

		var $button = $(this),
			data = {
				'action': 'loadmore',
				'query': loadmore_params.posts,
				'page' : loadmore_params.current_page,
				'template': $(this).data('template')
			};

		$.ajax({
			url : loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				$button.text('Loading...');
			},
			success : function( data ) {
				if( data ) {
					$button.text( 'More posts' ).prev().append(data); // insert new posts
					loadmore_params.current_page++;

					setTimeout( function () {
						$( '.ajax-item' ).addClass( 'in' );
					}, 200 );

					if ( loadmore_params.current_page >= loadmore_params.max_page ) {
						// $button.hide(); // if last page, hide the button
						$('.loadmore').addClass('d-none');
						$( document.body ).trigger( 'post-load' );
					} else { 
						$('.loadmore').removeClass('d-none');
					}
					lazy();
				} else {
					// $button.hide(); // if no data, hide the button as well
					$('.loadmore').addClass('d-none');
				}
			}
		});
	});
});