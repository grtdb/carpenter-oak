$( document ).ready( function () {

	//----------| scroll to top |----------//
	$( '#back-to-top' ).click( function () {
		$( 'html, body' ).animate( {
			scrollTop: 0
		}, 800 );
		return false;
	} );

	//----------| Search bar |----------//

	$('#searchBtn').on('click', function() {
		$('.main-nav').toggleClass('d-none');
		$('.search-bar').toggleClass('d-none');
		$('.search-bar #s').focus();
	});


	$(document).keyup(function(e) {
		if (e.keyCode == 27 && $('.search-bar').is(":visible") ) {
			$('.main-nav').removeClass('d-none');
			$('.search-bar').addClass('d-none');
		}
	});

	//----------| Header bar on scroll |----------//
	if ( isDesktop() ) {
		var lastScrollTop = 1000;
		$( window ).scroll( function ( event ) {
			var st = $( this ).scrollTop();
			if ( st < 5 ) {
				$( 'header' ).removeClass( 'scrolling' );
				$( 'header' ).removeClass( 'scroll-up' );
			} else if ( st > $( 'header' ).outerHeight() ) {
				$( 'header' ).addClass( 'scrolling' );
			}
			if ( st > lastScrollTop ) {
				if ( lastScrollTop > 5 || $( 'header' ).hasClass( 'scroll-up' ) ) {
					$( 'header' ).removeClass( 'scroll-up' );
					$( 'header' ).addClass( 'scroll-down' );
				}
			} else {
				if ( lastScrollTop > 5 || $( 'header' ).hasClass( 'scroll-down' ) ) {
					$( 'header' ).removeClass( 'scroll-down' );
					$( 'header' ).addClass( 'scroll-up' );
				}
			}
			lastScrollTop = st;
		} );
	}

	//----------| Mobile Nav |----------//

	$( '#mob-nav').click( function ( e ) {
		e.preventDefault();

		if ( $( this ).hasClass( 'active' ) ) {

			$( this ).removeClass( 'active' );
			$('header #menu').removeClass('more-menu');
			$( '.header nav' ).removeClass( 'active' );
			$( '.header' ).removeClass( 'active' );
			$('.header nav').removeClass('animate');
			$('.header nav').removeClass('animate');

			setTimeout(function(){
				$('.header nav').removeClass('more-menu');
			}, 100);

			$( 'body' ).removeClass( 'fixed' );

			//force mobile menu 'close' button to disspear if activated by 'more' button
			$('.mob-button').removeClass('active');
			$('#moreBtn').removeClass('active');	


		} else {
			$( this ).addClass( 'active' );
			$( '.header nav' ).addClass( 'active' );

			$( '.header' ).addClass( 'active' );
			$( 'body' ).addClass( 'fixed' );

		}

	} );



	if ( isMobile() || isTablet() ) {
		$('nav #menu.header li a').click(function(e){
			if( !$(this).parent().hasClass('active') && $(this).parent().hasClass('menu-item-has-children') ) {
				$(this).parent().siblings().removeClass('active');
				$(this).parent().addClass('active');
				$('.menu li').removeClass('active');
				e.preventDefault();
			} else {
				return true;
			}
		});
	}


	// team ajax filtering
	$( '.team-ajax-filter .ajax-item' ).addClass( 'in' );
	$( '.team-ajax-filter' ).click( function ( e ) {
		e.preventDefault();
		var filter = $( this );
		$.ajax( {
			url: filter.attr( 'href' ),
			beforeSend: function ( xhr ) {
				$( '.active' ).removeClass( 'active' );
				filter.addClass( 'active' );
				$( '.ajax-item' ).removeClass( 'in' );
				$( '.ajax-item' ).addClass( 'active' );
			},
			success: function ( data ) {
				var ajaxData = $( data ).find( '.ajax-item' );
				$( '.ajax-wrapper' ).html( ajaxData );
				setTimeout( function () {
					$( '.ajax-item' ).addClass( 'in' );
				}, 200 );
				window.history.pushState( "Details", "Title", filter.attr( 'href' ) );
			}
		} );
		return false;
	} );

	$( '.link-to-scroll' ).click( function (e) {
		e.preventDefault();
		var link = $( this ).attr( 'href' );
		$( 'html, body' ).animate( {
			scrollTop: $( link ).offset().top - 100
		}, 1000 );
	} );
	

	//-- hack to show background image on last side bar may be able to remove in future --//
	var set = $('.side-bar');
	var length = set.length;
	set.each(function(index, element) {
		if (index === (length - 1)) {
			$(this).addClass('side-bar__image');
		}
	});

	// add touch gestures to mobile versions of carousels
	$(".carousel").on("touchstart", function(event){
		var xClick = event.originalEvent.touches[0].pageX;
		$(this).one("touchmove", function(event){
			var xMove = event.originalEvent.touches[0].pageX;
			if( Math.floor(xClick - xMove) > 5 ){
				$(".carousel").carousel('next');
			}
			else if( Math.floor(xClick - xMove) < -5 ){
				$(".carousel").carousel('prev');
			}
		});
		$(".carousel").on("touchend", function(){
			$(this).off("touchmove");
		});
	});


	lazy();

	AOS.init({
		disable: 'mobile',
		startEvent: 'DOMContentLoaded',
		initClassName: 'aos-init',
		animatedClassName: 'aos-animate',
		offset: 100, // offset (in px) from the original trigger point
		delay: 0, // values from 0 to 3000, with step 50ms
		duration: 1000, // values from 0 to 3000, with step 50ms
		easing: 'ease-out-cubic', // default easing for AOS animations
		once: true, // whether animation should happen only once - while scrolling down
		mirror: false, // whether elements should animate out while scrolling past them
		anchorPlacement: 'top-bottom' // defines which position of the element regarding to window should trigger the animation
	});



	//lazy load carousel images
	$('#imageCarousel').on('slide.bs.carousel', function (e) {
		var $nextImage = null;
		$activeItem = $('.carousel-item.active', this);
		if (e.direction == 'left'){
			$nextImage = $activeItem.next('.carousel-item');
		} else {
			if ($activeItem.index() == 0){
				$nextImage = $('.carousel-item.last');
			} else {
				$nextImage = $activeItem.prev('.carousel-item');
			}
		}
		
		// prevents the loaded image if it is already loaded
		var src = $nextImage.data('lazy-load-src');

		if (typeof src !== "undefined" && src != "") {
			$nextImage.css({
				'background-image' : 'url(' + src + ')'
			});
			$nextImage.data('lazy-load-src', '');
		}
	});

	//added for project filters so it reloads when hitting back
	$(window).on('popstate', function() {
		location.reload(true);
	 });


	 $('.wpcf7-form-control-wrap.residential-vote, .wpcf7-form-control-wrap.commercial-vote').each(function(){
		 $img = $(this).parent().find('img');
		 $(this).parent().find('br').remove();
		 $(this).find('label').prepend($img);
	 })

} );

function lazy() {
	$('.lazy , .lazy-no-effect').Lazy( {
		effect: "fadeIn",
		effectTime: 1000,
		threshold: 500,
		afterLoad: function ( element ) {
			element.addClass( 'lazy-in' );
			element.prev('.lazy-loader').remove();
		}
	});
}

//----------| Testimonials to have the equal height |----------//

function sameHeight() {


	var maxHeight = 0;

	$('.sameheight').each( function() {
		$(this).removeAttr("style");
	});

	$('.sameheight').each( function() {
		if ($(this).height() > maxHeight) { 
			maxHeight = $(this).height();
		}
	});

	$('.sameheight').height(maxHeight);
};




$(window).on("load resize",function(e){

    sameHeight();

});