<?php
/** @var string $template */

global $wp_query;
// don't display the button if there are not enough posts
$active = (  $wp_query->max_num_pages > 1 ? '' : 'd-none'); ?>
	<div class="loadmore <?php echo $active; ?>" data-template="<?= $template; ?>">More posts</div>