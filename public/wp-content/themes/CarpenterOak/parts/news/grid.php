<script type="text/javascript">
	function instafeed() {
		if ( $('.insta-block').length > 0 ) {
			instafetch.init({
				accessToken: '3131299756.1677ed0.42fec61158b34e4692b2c5fa5d79950a',
				numOfPics: 20,
				caption: true,
				onSuccess: function onSuccess(json, options) {
					$('.insta-block').each( function(i) {
						var typeCount = $(this).data('type-count');
						var url      = json.data[typeCount].images.standard_resolution.url;
						var link     = json.data[typeCount].link;
						var username = json.data[typeCount].user.username;
						$(this).css('background-image', 'url(' + url + ')');
						$(this).attr('href', link);
						$(this).find('.username').html(username);
					} );
				}
			});
		}
	}

	function twitterfeed( requestVars ) {
		var request = $.extend( {}, requestVars );
		if ( $('.twitter-block').length > 0 ) {
			$.getJSON( '/wp-content/themes/CarpenterOak/parts/twitter.php', request, function ( data ) {
				$( '.twitter-block' ).each( function ( i ) {
					var typeCount = $( this ).data( 'type-count' );
					var text = data[ typeCount ].text;
					var link = '//twitter.com/carpenteroak/status/' + data[ typeCount ].id_str;
					$( this ).find( '.tweet' ).html( text );
					$( this ).attr( 'href', link );
				} );
			} );
		}
	}

	$(document).ready(function() {
		twitterfeed( {count: 4 } );
		instafeed();
	});
</script>
<?php
$insta     = $tweet = 0;
$blocks    = [];
$blockSets = [];
$i = $j = 0;

global $wp_query;
$post_count = $wp_query->post_count;

while ( have_posts() ) {
	the_post();

	$i++;

	$blocks[] = [
			'type'  => 'post',
			'title' => get_the_title(),
			'date'  => get_the_date('jS F Y'),
			'link'  => get_permalink(),
			'ID'    => get_the_ID()
	];

	if ( $i % ( $social ? 4 : 6 ) == 0 || ( $i === 3 && $post_count === 3 ) ) {
		$j++;
		if ( $social ) {
			if ($j % 2 == 0) {
				array_splice(
						$blocks,
						2,
						0,
						[
								[
										'type'  => 'insta',
										'count' => $insta++
								]
						]
				);
				$blocks[] = [
						'type'  => 'tweet',
						'count' => $tweet++
				];
			} else {
				array_splice(
						$blocks,
						3,
						0,
						[
								[
										'type'  => 'insta',
										'count' => $insta++
								],
								[
										'type'  => 'tweet',
										'count' => $tweet++
								]
						]
				);
			}
		}

		$blockSets[] = $blocks;
		$blocks = [];
	}
}

wp_reset_postdata();

foreach ( $blockSets as $blockSet ): ?>
	<div class="blockset">
		<div class="row">
			<?php
			for ( $blockIndex = 0;
			$blockIndex < 6;
			$blockIndex ++ ) {
			if ( $blockIndex == 0 ) {
			?>
			<div class="col-12 col-md-6">
				<div class="row">
					<?php
					} elseif ( $blockIndex == 2 || $blockIndex == 4 ) {
					?>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="row">
					<?php
					} elseif ( $blockIndex == 3 ) {
					?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="row">
					<?php
					}

					?>
					<div class="col-12">
						<?php
						if ( isset( $blockSet[ $blockIndex ] ) ) {
							$block         = $blockSet[ $blockIndex ];
							$half          = ( $blockIndex < 2 || $blockIndex > 3 );
							$blockTemplate = ( $half ? 'half' : 'full' ) . '-' . $block['type'];
							DbHelper::get_part(
                                    'news/' . $blockTemplate,
                                    [ 'half' => $half, 'block' => $block, 'blockIndex' => $blockIndex ]
							);
						}
						?>
					</div>
					<?php

					if ( $blockIndex == 5 ) {
					?>
				</div>
			</div>
			<?php
			}
			}
			?>
		</div>
	</div>
<?php endforeach; ?>

<div class="blockset">
	<div class="row">
		<?php
		foreach ( $blocks as $k => $block ) {
			?>
			<div class="col-12 col-md-6 post">
				<?php
				$blockTemplate = ( $half ? 'half' : 'full' ) . '-' . $block['type'];
				DbHelper::get_part(
                    'news/' . $blockTemplate,
                    [ 'half' => $half, 'block' => $block, 'blockIndex' => $k ]
				);
				?>
			</div>
			<?php
		}
		?>
	</div>
</div>