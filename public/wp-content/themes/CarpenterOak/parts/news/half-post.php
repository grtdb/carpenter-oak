<div class="row ajax-item half-post">
	<div class="col-md-6 order-2 <?= ( !($blockIndex % 2 == 0)) ? 'order-md-2' : 'order-md-1'; ?>">
		<a class="d-block" href="<?= $block['link']; ?>">
			<div class="news-block <?= ( $blockIndex % 2 == 0) ? 'bk-orange' : 'bk-grey'; ?> half">
				<h2><?= $block['title']; ?></h2>
				<p class="read-more append-arrow">Read more</p>
			</div>
		</a>
	</div>
	<div class="col-md-6 order-1 ">
		<a class="d-block news-block-image-wrap half" href="<?= $block['link']; ?>">
			<div class="news-block-image" style="background-image: url('<?= (get_the_post_thumbnail_url( $block['ID'], 'news-half-post')) ? get_the_post_thumbnail_url( $block['ID'], 'news-half-post') : get_template_directory_uri() . '/assets/img/no-img.gif'; ?>');"></div>

		</a>
	</div>
</div>