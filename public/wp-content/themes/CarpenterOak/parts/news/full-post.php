<div class="row ajax-item">
	<div class="col-12 full">
		<a href="<?= $block['link'];?>">
			<div class="news-block-seventy">
				<div class="news-block-image-wrap seventy">
					<div class="news-block-image" style="background-image: url('<?= (get_the_post_thumbnail_url( $block['ID'], 'news-full-post')) ? get_the_post_thumbnail_url( $block['ID'], 'news-full-post') : get_template_directory_uri() . '/assets/img/no-img.gif'; ?>');"></div>
				</div>
			</div>
			<div class="news-block-thirty">
				<div class="news-block bk-orange thirty">
					<h2><?= $block['title']; ?></h2>
					<p class="read-more append-arrow">Read more</p>
				</div>
			</div>
		</a>
	</div>
</div>
