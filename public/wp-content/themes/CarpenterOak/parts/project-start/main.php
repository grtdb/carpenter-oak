<div class="carousel-item">
	<div class="row project-start__set project-start__set--main">
		<div class="col-lg-12 left">
			<div class="content <?=$type;?>">
				<div class="row">
					<div class="col-12">
						<h4><?= $title; ?></h4>
						<a class="arrow arrow-left" href="#project-start__carousel" data-slide="prev"></a>
						<p>Pick one or more</p>
					</div>
					<?= $question; ?>
				</div>
			</div>
		</div>
	</div>
</div>
