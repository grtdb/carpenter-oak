<div class="carousel-item active">
	<div class="row project-start__set project-start__set--initial">
		<div class="col-lg-6 left" data-aos="fade-up">
			<div class="content">
				<h4>Start your project</h4>
				<p>Here at Carpenter Oak we live and breathe timber framed buildings, large and small, and we love a challenge. We create truly unique frames that respond to your brief giving you a bespoke 'made to measure' and personalised solution to your brief and budget.</p>

				<p>To start your journey with Carpenter Oak please provide a few details about your project using the form opposite.</p>
			</div>
		</div>
		<div class="col-lg-6 right" data-aos="fade-up">
			<div class="content <?=$type;?>">
				<h4><?= $title; ?></h4>
				<p>Please select below</p>
				<?= $question; ?>
			</div>
		</div>
	</div>
</div>
