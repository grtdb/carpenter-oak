<?php
$contact_form = wpcf7_contact_form( 795 );

$form_tags = $contact_form->scan_form_tags();

$titles = [
	'project-role'  => 'I am a...',
	'project-type'  => 'What type of project are you looking to build'
];

$i = 0;
foreach( $form_tags as $form_tag ) {
	if ( empty( $form_tag['options'] ) || !in_array('project-start', $form_tag['options'] ) ) {
		continue;
	}

	$key = $form_tag['name'];

	ob_start();

	dbHelper::get_part( 'project-start/types/' . $form_tag['type'], [
		'answers' => $form_tag['labels'],
		'key'     => $form_tag['name'],
	] );

	if ( $i === 0 ) {
		dbHelper::get_part( 'project-start/initial', [
			'question' => ob_get_clean(),
			'type' => $key,
			'title'    => ( array_key_exists( $key, $titles ) ) ? $titles[ $key ] : '', ]			
		);
	} else {
		dbHelper::get_part( 'project-start/main', [
				'question' => ob_get_clean(),
				'type' => $key,
				'title'    => ( array_key_exists( $key, $titles ) ) ? $titles[ $key ] : '', ]
		);
	}

	++$i;
}