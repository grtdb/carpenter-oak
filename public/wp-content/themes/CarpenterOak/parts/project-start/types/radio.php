<?php
/** @var array $answers */
/** @var string $key */
?>
<input type="hidden" name="<?= $key; ?>" value="" />
<?php foreach( $answers as $answer ): ?>
    <div class="project-start__option">
        <a class="btn append-arrow" data-value="<?= $answer; ?>"><?= $answer; ?></a>
    </div>
<?php endforeach; ?>
