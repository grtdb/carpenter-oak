
<?php
//if ( have_posts() ) : ?>

<?php

	$grid_original        = array('col-md-6 first', 'col-md-6 second', 'col-md-6 third', 'col-md-12 fourth', 'col-md-6 fifth', 'col-md-6 sixth', 'col-md-6 seventh', 'col-md-12 eight');
	$blockHeight_original = array('full-col', 'half-row', 'half-row', 'full-col half', 'half-row', 'half-row', 'full-col', 'full-col');
	$imageSizes_original  = array('grid-half-full', 'grid-half-half', 'grid-half-half', 'grid-full', 'grid-half-half', 'grid-half-half', 'grid-half-full', 'grid-full' );

	//used to quickly extend above arrays
	$desiredLength = 80;
	$grid = array();
	$blockHeight = array();
	$imageSizes = array();

	// create a new array with AT LEAST the desired number of elements by joining the array at the end of the new array
	while(count($grid) <= $desiredLength){
	    $grid = array_merge($grid, $grid_original);
	}

	while(count($blockHeight) <= $desiredLength){
	    $blockHeight = array_merge($blockHeight, $blockHeight_original);
	}

	while(count($imageSizes) <= $desiredLength){
		$imageSizes = array_merge($imageSizes, $imageSizes_original);
	}

	// reduce the new array to the desired length (as there might be too many elements in the new array
	$grid = array_slice($grid, 0, $desiredLength);
	$blockHeight = array_slice($blockHeight, 0, $desiredLength);
	$imageSizes = array_slice($imageSizes, 0, $desiredLength);

	$i = 0;

//	 If a half block is the last element and the one before is a full then make the last one a full as well
	$lastBlock       = num_posts() - 1;
	$secondLastBlock = num_posts() - 2;

	if (
			isset($blockHeight[$lastBlock], $blockHeight[$secondLastBlock]) &&
			$blockHeight[$lastBlock] === 'half-row' && $blockHeight[$secondLastBlock] === 'full-col'
	) {
		$blockHeight[$lastBlock] = 'full-col';
	}


	while ( have_posts() ) : the_post();

		$k = $i-1;
		$j = $i+1;

		if (has_post_thumbnail(get_the_ID())):
			$project_image = get_the_post_thumbnail_url(get_the_ID(),$imageSizes[$i]);

		else:

			if( have_rows('project_gallery', get_the_ID()) ):
				$j = 0;

				while ( have_rows('project_gallery', get_the_ID()) ) : the_row();
					if ($j == 0) {

						$project_image = get_sub_field('project_gallery_image', get_the_ID());
						$project_image = $project_image['url'];
						$j++;
					}
				endwhile;
			endif;
		endif; 

		// Build our array with the data we need using key => value
		$projects_data[] = array(
			'title' => get_the_title(get_the_ID()),
			'category' => wp_get_post_terms(get_the_ID(), 'project-category', array("fields" => "names")),
			'sub_title'  => get_field('project_sub_title', get_the_ID()),
			'image_url' => $project_image,
			'post_link' => get_permalink(get_the_ID())
		);


		if ($blockHeight[$i] == 'half-row') :
			if ($blockHeight[$k] == 'half-row' && $blockHeight[$i] == 'half-row') :
				?>

				<div class="<?php echo $grid[$k]; ?> ajax-item">
					<a href="<?php echo $projects_data[$k]['post_link']; ?>" class="project" data-aos="fade-up">
						<div class="content">
							<h4><?php echo $projects_data[$k]['title']; ?></h4>
							<h5><?php echo $projects_data[$k]['sub_title']; ?></h5>
						</div>
						<p class="view-project-link"><span class="append-arrow">View project</span></p>
						<div class="overlay"></div>
						<span class="lazy-loader"></span>
						<div class="image lazy <?php echo $blockHeight[$k]; ?>" data-src="<?= $projects_data[$k]['image_url']; ?>"></div>
					</a>
					<a href="<?php echo $projects_data[$i]['post_link']; ?>" class="project" data-aos="fade-up">
						<div class="content">
							<h4><?php echo $projects_data[$i]['title'];  ?></h4>
							<h5><?php echo $projects_data[$i]['sub_title']; ?></h5>
						</div>
						<p class="view-project-link"><span class="append-arrow">View project</span></p>
						<div class="overlay"></div>
						<span class="lazy-loader"></span>
						<div class="image lazy <?php echo $blockHeight[$i]; ?>" data-src="<?= $projects_data[$i]['image_url']; ?>"></div>
					</a>
				</div>
				<?php

			endif;

		 else :
		 ?>

			<div class="<?php echo $grid[$i]; ?> ajax-item">
				<a href="<?php echo $projects_data[$i]['post_link']; ?>" class="project" data-aos="fade-up">
					<div class="content">
						<h4><?php echo $projects_data[$i]['title']; ?></h4>
						<h5><?php echo $projects_data[$i]['sub_title']; ?></h5>
					</div>
					<p class="view-project-link"><span class="append-arrow">View project</span></p>
					<div class="overlay"></div>
					<span class="lazy-loader"></span>
					<div class="image lazy <?php echo $blockHeight[$i]; ?>" data-src="<?= $projects_data[$i]['image_url']; ?>"></div>
				</a>
			</div>

			<?php

		endif;

		$i++; 
		
	endwhile; 
	wp_reset_postdata();  

?>

<?php //else : ?>

<?php //endif; ?>
