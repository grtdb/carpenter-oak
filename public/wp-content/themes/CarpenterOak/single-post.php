<?php /* Single Post Template */
get_header();
the_post();
$page_id = get_the_ID();
get_hero();?>

	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		 "@type": "BlogPosting",
		 "headline": "<?= get_the_title($page_id);  ?>",
		 "image": "<?= get_the_post_thumbnail_url($page_id,'full'); ?>",
		 "url": "<?= get_permalink();?>",
		 "datePublished": "<?= get_the_date('Y-m-d'); ?>",
		   "author": {
		    "@type": "Person",
		    "name": "<?= get_the_author(); ?>"
		  }
	 }
</script>
	<section class="section__text">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3 sub-menu side-bar">
					<a class="back append-arrow-left" href="<?=  get_post_type_archive_link( 'post' ); ?>">Back</a>
					<?php /* the_sub_menu(); */ ?>
				</div>
				<div class="col-md-9 sub-content post-content">
					<span class="date">Posted on: <?= get_the_date('jS F Y'); ?></span>
					<div class="cms clearfix">
						<?php the_content(); ?>
					</div>
					<div class="share-links">
						<h4 class="text-orange d-inline-block">Share this post</h4>
						<span class="icon__social icon__social--twitter st-custom-button" data-network="twitter"></span>
						<span class="icon__social icon__social--facebook st-custom-button" data-network="facebook"></span>
						<span class="icon__social icon__social--linkedin st-custom-button" data-network="linkedin"></span>
						<span class="icon__social icon__social--pinterest st-custom-button" data-network="pinterest"></span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php //TODO put into own part ?>

	<?php

		//get ID of posts page;
		$page_for_posts = get_option( 'page_for_posts' );

	?>
	<section class="container-fluid title-block">
		<div class="row">
			<div class="col">
				<h3>Related posts</h3>
				<a href="<?php echo get_permalink($page_for_posts); ?>" class="append-arrow">View all posts</a>
			</div>
		</div>
	</section>

	<?php 

		$categories = wp_get_post_categories( get_the_ID() );

		$args = [
			'post_type'      => 'post',
			'posts_per_page' => '3',
			'max_num_pages'  => '1',
			'post__not_in'   => [ get_the_ID() ],
			'orderby'   => 'publish_date',
			'order' => 'DESC',
		];

		query_posts( $args );
	?>

	<section class="news container-fluid">
		<div class="ajax-wrapper">
			<?php DbHelper::get_part( 'news/grid', [ 'social' => false ] ); ?>
		</div>
	</section>

<?php get_footer();