<?php get_header(); ?>
<?php get_hero('search'); ?>
<?php $page_id = get_the_ID(); ?>

<section class="section__search">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 text-center sub-content">
				<h2 class="text-orange">Your search for "<?php echo get_search_query(); ?>" returned the following results</h2>
			</div>
		</div>

		<?php 

		if( have_posts()) :
			$types = array('project', 'post', 'page' );
			foreach( $types as $type ) :

				// if( $type == get_post_type()) :

					?>

					<div class="row">
						<div class="col-md-12">
							<h3 class="text-orange border-bottom"><?php echo $type; ?>s</h3>
						</div>
					</div>

					<?php

				// endif;

				while( have_posts()) :
					the_post();

					if( $type == get_post_type()) :
						?>

						<div class="row">
							<div class="col-md-4 col-lg-3">
								<?php 

								$post_id = get_the_ID();

								if (has_post_thumbnail($post_id)):
									$image = get_the_post_thumbnail_url($post_id,'full');

								else:

									if( have_rows('project_gallery', $post_id) ):
										$j = 0;

										while ( have_rows('project_gallery', $post_id) ) : the_row();
											if ($j == 0) :

												$image = get_sub_field('project_gallery_image', $post_id);
												$image = $image['url'];
												$j++;
											endif;

										endwhile;

									endif;

								endif;

								if ($image) :
									echo '<div class="image" style="background-image: url(' . $image . '); width: 100%; max-width: 100%;"></div>';
								endif;

								?>
							</div>
							<div class="col-md-8 col-lg-9">
								<h4 class="<?php echo $type; ?>"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h4>

								<?php

								if (get_field('project_sub_title', $post_id)) {
									echo '<h5>' . get_field('project_sub_title', $post_id) . '</h5>';
								}

								if (get_field('project_short_overview', $post_id)) {
									echo '<p>' . wp_trim_words(get_field('project_short_overview', $post_id), 55, ' […]' ) . '</p>';
								}

								if ($type == 'post') {
									echo '<h5>' . get_the_date() . '</h5>';
								}

								echo get_the_excerpt();

								// check if page has text block section to fill search result with content
								if( have_rows('section') ):

								    // loop through the rows of data
									while ( have_rows('section') ) : the_row();

										if( get_row_layout() == 'fc_text' ): 

											echo '<p>' . wp_trim_words(get_sub_field('fc_text_block', $post_id), 55, ' […]' ) . '</p>';

										endif;

									endwhile;

								endif;
								?>

								<p><a class="btn append-arrow" href="<?php echo get_permalink(); ?>">Read more</a></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 border-bottom hr">
							</div>
						</div>

						<?php

					endif;
				endwhile;
				rewind_posts();

			endforeach;
		endif;
?>


</div>	
</section>



<?php get_footer(); ?>