<?php /* Blog Page */
get_header();
$page_id = get_option( 'page_for_posts' );
get_hero('news');

?>


	<?php //TODO put into own part ?>
	<section id="sub-menu" class="bg-white">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<nav>
						<ul id="menu" class="sub">
							<li>
								<a href="<?= get_permalink( get_option( 'page_for_posts' ) ); ?>" class="ajax-filter active" data-template="news/grid">All</a>
							</li>
							<?php
							$terms = get_terms( 'category', 'orderby=name' );
							if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
								foreach ( $terms as $term ) {
									echo '<li><a data-template="news/grid" data-termid="' . $term->term_id . '" data-posttype="' . get_post_type() . '" class="ajax-filter ' . $active . '" href="' . get_term_link( $term ) . '">' . $term->name . '</a></li>';
								}
							}
							?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>

	<section class="news container-fluid">
		<div class="ajax-wrapper">
			<?php DbHelper::get_part( 'news/grid', ['social' => TRUE] ); ?>
		</div>
		<?php DbHelper::get_part( 'loadmore', ['template' => 'news/grid'] ); ?>
	</section>


<?php get_footer();
