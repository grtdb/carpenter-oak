<?php /* Custom menus */

register_nav_menus( array(
	'main_menu' => 'Main',
	'footer_menu' => 'Footer',
	'footer_about' => 'Footer About',
	'footer_experts' => 'Footer Experts',
	'footer_projects' => 'Footer Projects',
	'footer_locations' => 'Footer Locations',


) );

// Filter wp_nav_menu() to add additional 'more...' button
function add_more_menu_item($items, $args) {

	$menu_items = wp_get_nav_menu_items('Main'); 

	foreach((array) $menu_items as $key => $item) :

		$hide_nav_item = get_field('hide_nav_menu_item', $item);
		
		if( $hide_nav_item ) :

			$hidden_menu_items .= '<li><a href="'.$item->url.'" title="'.$item->title.'">'.$item->title.'</a></li>';
			
		endif;
		
	endforeach;

	
	if ($args->theme_location == 'main_menu' && $hidden_menu_items) {

		$moreLink = '<li class="menu-item more-btn d-none d-lg-block d-xl-none menu-item-has-children"><a title="More" href="#" id="moreBtn">' . esc_html__('More...', 'carpenter-oak') . '</a><ul class="sub-menu">' . $hidden_menu_items . '</ul></li>';

		$items = $items . $moreLink;

		return $items;

	} else {

		return $items;

	}
}

add_filter( 'wp_nav_menu_items', 'add_more_menu_item', 10, 2);

// Filter wp_nav_menu() to add search button
function add_search_icon($items, $args) {
	if ($args->theme_location == 'main_menu') {
		$searchLink = '<li class="menu-item search-btn"><span title="Search Carpenter Oak" id="searchBtn">' . esc_html__('Search', 'carpenter-oak') . '</span></li>';
		// $searchLink = '<li class="menu-item search-btn">' . get_search_form( false ) . '</li>';
		$items = $items . $searchLink;
		return $items;
	} else {
		return $items;
	}
}

add_filter( 'wp_nav_menu_items', 'add_search_icon', 10, 2);


function my_wp_nav_menu_objects( $items, $args ) {

	foreach( $items as &$item ) :

		$hide_nav_item = get_field('hide_nav_menu_item', $item);
		
		if( $hide_nav_item ) :
			
			array_push( $item->classes, 'd-block', 'd-lg-none', 'd-xl-block' );
			
		endif;
		
	endforeach;

	return $items;
	
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);


