<?php
function get_hero($placement = '') {

	$page_id = get_the_ID();
	$page = get_page_by_title('Project Gallery');

	if ($placement == 'team') :
		$background_image = get_field('team_feature_image', 'option');
		$background_image_url = $background_image['url'];
	elseif ($placement == 'news') :
		$page_id = get_option( 'page_for_posts' );
		$background_image_url = get_the_post_thumbnail_url($page_id,'full');
	elseif (has_post_thumbnail($page_id)) :
	 	$background_image_url = get_the_post_thumbnail_url($page_id,'full');
	elseif ($placement == 'search') :
		$background_image_url = get_template_directory_uri() . '/assets/img/search-bg.webp';
	elseif ($page_id == $page->ID) :
		$background_image_url = '';
	else :
		$background_image_url = get_template_directory_uri() . '/assets/img/default-hero-bg.jpg';
	endif;



	if ($background_image_url) :
		?>
		<section class="section__hero" style="background-image: url(<?php echo $background_image_url; ?>);">
			<?php

			if ($placement == 'search') :

				?>

				<h1>Search</h1>
			
			<?php 

			else : 

				?>

				<h1><?php echo get_the_title($page_id); ?></h1>

			<?php 
		endif;

		?>
		</section>
		<?php
	endif;

}
?>