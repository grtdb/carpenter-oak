<?php

// show the 'Global settings option page'
if( function_exists('acf_add_options_page') ) {
	
	$args = array(
		'page_title' => 'Global site settings for Carpenter Oak',
		'menu_title' => 'Globals',
		'menu_slug' => 'global-settings',
		'capability' => 'edit_posts',
		'update_button'		=> __('Update', 'acf'),
		'updated_message'	=> __("Settings Updated", 'acf'),

	);

	acf_add_options_page($args);

	$args2 = array(
        'page_title'  => 'Team settings',
        'menu_title'  => 'Team settings',
        'menu_slug'   => 'team-settings',
        'parent_slug' => 'edit.php?post_type=team',
        'capability'  => 'manage_options',
        'position'  => false,
        'icon_url' => false,
      );

    acf_add_options_page($args2);


}

