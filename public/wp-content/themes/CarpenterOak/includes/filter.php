<?php
function filter_scripts() {
	// register our main script but do not enqueue it yet
	wp_register_script( 'filter', get_stylesheet_directory_uri() . '/assets/js/src/jquery/filter.js', [ 'jquery' ], '3.3' );

	// Localise script to inject page data
	wp_localize_script( 'filter', 'filter_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
	] );

	wp_enqueue_script( 'filter' );
}

add_action( 'wp_enqueue_scripts', 'filter_scripts' );

function filter_ajax_handler() {
	global $wp_query;

	wp_reset_query();

	$term_id       = $_POST['term_id'];
	$term          = get_term( $term_id );
	$term_taxonomy = $term->taxonomy;

	$tax_query[] = [
		'taxonomy' => $term_taxonomy,
		'field'    => 'term_id',
		'terms'    => [ $term_id ]
	];

	parse_str( $_POST['filters'], $filters );
	if ( isset( $filters['filter'] ) ) {
		$filters = $filters['filter'];

		foreach( $filters as $term_taxonomy => $term_ids ) {
			$tax_query[] = [
				'taxonomy' => $term_taxonomy,
				'field'    => 'term_id',
				'terms'    => $term_ids,
			];
		}
	}

	
	if ( $term_taxonomy == 'building-type' || $term_taxonomy == 'project-category' ) {
		$posts_per_page = '-1';
	} else {
		$posts_per_page = get_option('posts_per_page');
	}
	
	$args = [
		'post_type'      => $_POST['post_type'],
		'posts_per_page' => $posts_per_page,
		'post_status'    => 'publish',
		// 'tax_query'      => $tax_query,
		'orderby'   => 'menu_order',
		'order'     => 'ASC'
	];

	$conditional_tax_query = array(
		'tax_query'  => $tax_query,
	);

	if ($term_id) :

	$args = array_merge($args, $conditional_tax_query);

endif;

	query_posts( $args );

	$return = [];

	if ( empty( $_POST['template'] ) ) {
		$return['error'] = 'No "template" var passed in loadmore part get';
	} else {
		$return['posts']        = DbHelper::get_part( $_POST['template'], [], false );
		$return['title']        = $term->name;
		$return['content']      = $term->description;
		$return['query']        = json_encode( $wp_query->query_vars );
		$return['current_page'] = $wp_query->paged ? $wp_query->paged : 1;
		$return['max_page']     = $wp_query->max_num_pages;
		
	}
	echo json_encode( $return );
	die;
}

add_action( 'wp_ajax_filter', 'filter_ajax_handler' );
add_action( 'wp_ajax_nopriv_filter', 'filter_ajax_handler' );