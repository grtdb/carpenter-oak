<?php /* -- Editor Style Selector -- */

function mce_custom_styles( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter('mce_buttons_2', 'mce_custom_styles');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'Lead Text',
			'block' => 'div',
			'classes' => 'lead',
			'wrapper' => true,

		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

// Add custom styling to editor to display styles
function add_custom_editor_styles() {

//	if ( dbHelper::is_env( 'dev' ) ) {
//		$url = get_template_directory_uri() . '/assets/css/src/admin.css';
//	} else {
//		$url = get_template_directory_uri() . '/assets/css/dist/admin.min.css';
//	}
//
//    add_editor_style( $url );
}
add_action( 'admin_init', 'add_custom_editor_styles' );