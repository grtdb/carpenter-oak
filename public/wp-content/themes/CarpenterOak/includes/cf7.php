<?php
require(__DIR__.'/../../../../../vendor/autoload.php');

use GGDX\PhpInsightly\Insightly;

// define the wpcf7_mail_sent callback
function action_wpcf7_mail_sent( $contact_form ) {
	$acf_insightly_mappings = get_field( 'insightly_mappings', 'options' );
	$api_key                = get_field( 'insightly_api_key', 'options' );

	$mapped_forms = get_mapped_forms( $acf_insightly_mappings );

	if ( empty( $api_key ) || !$mapped_forms || !in_array( $contact_form->id, $mapped_forms['forms'] ) ) return;

	$form_data = get_form_data( $mapped_forms['fields'][ $contact_form->id ] );

	$Insightly = new Insightly( $api_key );

	if ( !empty( $form_data ) ) {
		/** @var \GuzzleHttp\Psr7\Response $response */
		// Email on Exception
		try {
			$Insightly->saveLead( $form_data );
		} catch ( \GuzzleHttp\Exception\ClientException $e ) {
			if ( $admin_email = get_option( 'db-admin_email' ) ) {
				wp_mail( $admin_email, 'Insightly Error', $e->getResponse()->getBody()->getContents() );
			}
		}
	}
};

function get_mapped_forms( $acf_insightly_mappings ) {

	if ( empty( $acf_insightly_mappings ) ) return false;

	$return = [ 'forms' => [], 'fields' => [] ];

	foreach( $acf_insightly_mappings as $mapping ) {
		$return['forms'][] = $mapping['cf7_form_id'];

		$fields = [];
		foreach( $mapping['fields'] as $field ) {
			$fields[ $field['insightly_field'] ] = $field['cf7_field'];
		}

		$return['fields'][ $mapping['cf7_form_id'] ] = $fields;
	}
	return $return;
}

function get_form_data( $fields ) {
	$submission = WPCF7_Submission::get_instance();

	if ( $submission ) {
		$posted_data = $submission->get_posted_data();

		$form_data = [ 'customfields' => [] ];

		foreach( $fields as $key => $cf7_field ) {
			if ( isset( $posted_data[ $cf7_field ] ) ) {

				if ( is_array( $posted_data[ $cf7_field ] ) && count( $posted_data[ $cf7_field ] ) === 1 ) {
					$value = $posted_data[ $cf7_field ][0];

					// We have multi choice or a checkbox so take empty as false 1 as true
					if ( $value === '1' ) {
						$value = true;
					} elseif( $value === '' && strpos( $cf7_field, 'dropdown' ) !== false ) {
						// This is when we have a checkbox but it's actually a dropdown with expected values of Yes/No
						$value = 'No';
					} elseif ( $value === '') {
						$value = false;
					}

				} else {
					$value = $posted_data[ $cf7_field ];
				}

				if ( $value === '' ) continue;

				if ( strpos( $key, '__c' ) !== false || strpos( strtoupper( $key ), 'LEAD_FIELD_' ) !== false ) {
					$form_data['customfields'][] = [ 'CUSTOM_FIELD_ID' => $key, 'FIELD_VALUE' => $value ];
				} else {
					$form_data[ $key ] = $value;
				}

			}
		}
	}

	return $form_data;
}

// add the action
add_action( 'wpcf7_mail_sent', 'action_wpcf7_mail_sent', 10, 1 );