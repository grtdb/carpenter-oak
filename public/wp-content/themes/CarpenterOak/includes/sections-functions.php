<?php

global $post;
$page_id = get_the_ID();

function get_section_testimonial() {

	$project_testimonial = FALSE;

	// from acf flexible content 'sections'
	if (get_sub_field('fc_testimonial_object')) :
	$post_testimonials = get_sub_field('fc_testimonial_object');

	// from projects CPT
	elseif (get_field('project_testimonials')) :
		$post_testimonials = get_field('project_testimonials', $page_id);
		$project_testimonial = TRUE;
	endif;

	if( $post_testimonials ):
		$count = count($post_testimonials);
		$i = 0;
		?>

		<section class="section__testimonial" id="testimonial">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<div class="col">
						<div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<?php
								foreach ($post_testimonials as $post_testimonial):
									setup_postdata($post_testimonial);
									$project_link = get_field( 'testimonial_project_links', $post_testimonial );
										?>
										<div class="carousel-item<?php echo ($i == 0) ? ' active' : ''; ?> sameheight">
											<div class="quote">
												<p><?php echo get_field('testimonial_content', $post_testimonial); ?>	</p>
												<h4 class="text-orange"><?php echo get_the_title($post_testimonial); ?></h4>
											</div>


											<?php if ($project_testimonial == FALSE && $project_link) {
												?>
												<p class="text-right"> <a href="<?= get_permalink($project_link[0]->ID); ?>" class="append-arrow">View project</a></p>
												<?php
											}
											?>

										</div>
										<?php

										$i++;
								endforeach;
								?>
							</div>
							<?php if ($count >= 2) : ?>
								<a class="carousel-control-prev" href="#testimonialCarousel" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#testimonialCarousel" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							<?php endif; ?>

							</div>

						</div>

					</div>
				</div>
				<script>
					var carousel3 = $('#testimonialCarousel').carousel({interval: false});
				</script>
			</section>


		<?php
		wp_reset_postdata();

	endif;


	?>



	<?php
}

function get_section_text() {

	$post_text = get_sub_field('fc_text_block', $page_id);
	$post_text = apply_filters('the_content', $post_text);

	if( $post_text ):
			?>

			<section class="section__text">
				<div class="<?= container_width(); ?>">
					<div class="row">
						<?php sidebar_menu_setup(); ?>
							<?php echo $post_text; ?>
						</div>
					</div>
				</div>
			</section>

			<?php
	endif;
}

function get_section_video() {

	$post_video_source = get_sub_field('video_source', $page_id);
	$post_video_source_url = get_sub_field('video_source_url', $page_id);

	if ($post_video_source_url) :

		if ($post_video_source == 'vimeo') :

			?>

			<section class="section__video">
				<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/<?php echo $post_video_source_url; ?>?title=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
			</section>

			<?php

		else :

			?>

			<section class="section__video">
				<div style="padding:56.25% 0 0 0;position:relative;"><iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $post_video_source_url; ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>
			</section>

			<?php

		endif;
	endif;
}

function get_section_faq() {

	if( have_rows('fc_faq_questionsanswers', $page_id) ):
		$i = 0;
		?>

		<section class="section__faq">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<?php sidebar_menu_setup(); ?>
						<?php

						while ( have_rows('fc_faq_questionsanswers', $page_id) ) : the_row();
							?>
							<a class="faq--question" data-toggle="collapse" href="#collapseFaqQuestion<?php echo $i; ?>" role="button" aria-expanded="false" aria-controls="collapseFaqQuestion<?php echo $i; ?>">
								<?php echo get_sub_field('fc_faq_questionsanswers_question', $page_id); ?>
							</a>
							<div class="collapse" id="collapseFaqQuestion<?php echo $i; ?>">
								<div class="faq--answer">
									<?php echo get_sub_field('fc_faq_questionsanswers_answer', $page_id);
									?>
								</div>
							</div>
							<?php
							$i++;
						endwhile;

						?>
					</div>
				</div>
			</div>
		</section>

		<?php
	endif;
}

function get_section_project_start() {
	?>

	<section class="section__project-start">
		<div class="<?= container_width(); ?> project-start__container">
			<div id="project-start__carousel" class="carousel slide" data-interval="false" data-ride="false">
				<div class="carousel-inner">
					<form action="<?= get_permalink( get_page_by_path( 'contact' ) ); ?>" method="post">
						<?php dbHelper::get_part('project-start/question-sets'); ?>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php
}

function get_section_building_type() {
	$terms = get_terms([
		'taxonomy' => 'building-type',
	    // 'hide_empty' => false,
	]);

	if ($terms) :
		$i = 1;

		?>

		<section id="building-type">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<?php

					foreach ( $terms as $term) {

						$image = get_field('archive_feature_image', $term);
						$image = $image['url'];

						if ($i % 2 !== 0) :
							?>
							<div class="col-md-6 image-first" data-aos="fade-up">
								<a href="<?php echo get_term_link($term->term_id); ?>" class="type-block">
									<span class="lazy-loader"></span>
									<div class="type-image lazy" data-src="<?= $image; ?>"></div>
									<div class="type-content">
										<h2><?php echo $term->name; ?></h2>
										<span class="append-arrow white-arrow">View projects</span>
									</div>
								</a>
							</div>
							<?php

						else :

							?>
							<div class="col-md-6 image-last" data-aos="fade-up">
								<a href="<?php echo get_term_link($term->term_id); ?>" class="type-block">
									<div class="type-content">
										<h2><?php echo $term->name; ?></h2>
										<span class="append-arrow white-arrow">View projects</span>
									</div>
									<span class="lazy-loader"></span>
									<div class="type-image lazy" data-src="<?= $image; ?>"></div>
								</a>
							</div>
							<?php

						endif;
						$i++;
					}
					?>
				</div>
			</div>
		</section>

		<?php

	endif;
}

function get_section_cta() {

	// check for left column CTA
	if (get_sub_field('fc_cta_left_column')) :
		$post_left_ctas = get_sub_field('fc_cta_left_column');
	elseif (get_field('cta_left_column')) :
		$post_left_ctas = get_field('cta_left_column');
	endif;

	// check for right column CTA
	if (get_sub_field('fc_cta_right_column')) :
		$post_right_ctas = get_sub_field('fc_cta_right_column');
	elseif (get_field('cta_right_column')) :
		$post_right_ctas = get_field('cta_right_column');
	endif;


	if ($post_left_ctas || $post_right_ctas) :

		$awards_cat_id =  get_cat_ID('awards');
		$awards_cat_url = get_category_link($awards_cat_id);

	?>



	<section class="section__cta" data-aos="fade-up">
		<div class="<?= container_width(); ?>">
			<div class="row">
				<div class="col-lg-6 awards">
					<h3 class="text-orange">Awards</h3><a href="<?php echo $awards_cat_url; ?>" class="append-arrow">Read more</a>
				</div>

				<?php
					$ContactPopup = get_page_by_title('Contact', 'ARRAY_A', 'popupbuilder');
					$i = 0;

					if( $post_left_ctas ): ?>
						<?php foreach( $post_left_ctas as $post_cta):
							if ($i == 0) {
								?>
								<?php setup_postdata($post_cta); ?>
								<?php
								$title = get_the_title($post_cta);
								if ( $title === 'Contact') {
									?>
									<a class="col-md-6 col-lg-3 bk-orange text-white block-link sg-show-popup sgpb-popup-id-<?= $ContactPopup['ID']?>" data-sgpbpopupid="<?= $ContactPopup['ID']?>" data-popup-event="click">
									<?php
								} else {
									$customLink = get_field('cta_custom_link', $post_cta );
									if ( !empty($customLink)) {
									?>
										<a href="<?= $customLink ?>" class="col-md-6 col-lg-3 bk-orange text-white block-link">
									<?php
									} else {
									?>
										<a href="<?= get_permalink( get_field( 'cta_page_link', $post_cta ) ); ?>" class="col-md-6 col-lg-3 bk-orange text-white block-link">
									<?php
									}
									?>

									<?php
								}
								?>
									<h4><?= get_the_title($post_cta); ?></h4>
									<p><?= get_field('cta_message', $post_cta); ?></p>
									<p><span class="append-arrow"><?php echo get_field('cta_button_text', $post_cta); ?></span></p>
								</a>
								<?php
							}
							$i++;
						endforeach;
						wp_reset_postdata();
					endif;

					$i = 0;

					if( $post_right_ctas ): ?>
						<?php foreach( $post_right_ctas as $post_cta):
							if ($i == 0) {
								?>
								<?php setup_postdata($post_cta); ?>

								<?php
								$title = get_the_title($post_cta);
								if ( $title === 'Contact') {
									?>
									<a class="col-md-6 col-lg-3 bk-black text-white block-link sg-show-popup sgpb-popup-id-<?= $ContactPopup['ID']?>" data-sgpbpopupid="<?= $ContactPopup['ID']?>" data-popup-event="click">
									<?php
								} else {
									$customLink = get_field('cta_custom_link', $post_cta );
									if ( !empty($customLink)) {
									?>
										<a href="<?= $customLink ?>" class="col-md-6 col-lg-3 bk-black text-white block-link">
									<?php
									} else {
									?>
										<a href="<?= get_permalink( get_field( 'cta_page_link', $post_cta ) ); ?>" class="col-md-6 col-lg-3 bk-black text-white block-link">
									<?php
									}
								}
								?>
									<h4><?= get_the_title($post_cta); ?></h4>
									<p><?= get_field('cta_message', $post_cta); ?></p>
									<p><span class="append-arrow"><?php echo get_field('cta_button_text', $post_cta); ?></span></p>
								</a>
								<?php
							}
							$i++;
						endforeach;
						wp_reset_postdata();
					endif;

				?>

			</div>
		</div>
	</section>

	<?php

endif;
}

function get_inline_section_cta() {

	$ctas = get_sub_field('cta_items');
	if (!empty($ctas)) {
		?>
		<section class="section__cta section__cta--inline" data-aos="fade-up">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<?php
					$ContactPopup = get_page_by_title('Contact', 'ARRAY_A', 'popupbuilder');
					$i = 0;
					foreach ($ctas as $cta) {
						setup_postdata($cta); ?>
						<?php
						$title = get_the_title($cta);
						$col = 12 / get_sub_field('no_cta_items');
						$bgImage = get_field('cta_background_image', $cta);
						if ($title === 'Contact') {
						?>
							<a class="col-md-<?= $col; ?> block-link sg-show-popup sgpb-popup-id-<?= $ContactPopup['ID'] ?>" data-sgpbpopupid="<?= $ContactPopup['ID'] ?>" data-popup-event="click" style="background-image:url(' <?= (!empty($bgImage)) ? $bgImage['sizes']['news-full-post'] : ''; ?> ')">
						<?php
						} else {
							$customLink = get_field('cta_custom_link', $cta);

							if (!empty($customLink)) {
							?>
								<a href="<?= $customLink ?>" class="col-md-<?= $col; ?> block-link" target="_blank" style="background-image:url(' <?= (!empty($bgImage)) ? $bgImage['sizes']['news-full-post'] : ''; ?> ')">
							<?php
							} else {
							?>
								<a href="<?= get_permalink(get_field('cta_page_link', $cta)); ?>" target="_blank" class="col-md-<?= $col; ?> block-link" style="background-image:url(' <?= (!empty($bgImage)) ? $bgImage['sizes']['news-full-post'] : ''; ?> ')">
							<?php
							}
						}
						?>
						<p><?= get_field('cta_message', $cta); ?></p>
						</a>
						<?php
						$i++;
					}
					wp_reset_postdata();
					?>
				</div>
			</div>
		</section>
	<?php
	}
}

function get_section_office() {
	?>

	<section class="section__office">
		<div class="<?= container_width(); ?>">
			<div class="row">
				<?php

				sidebar_menu_setup();
				echo get_sub_field('fc_office_introduction', $page_id	);

				$post_location = get_sub_field('fc_office_location', $page_id);

				if( $post_location ):

					setup_postdata( $post_location );

					?>

					<div class="row">
						<div class="col-md-6">
							<p class="text-orange">Address</p>
							<?php echo get_field('location_address', $post_location);?>
							<p class="call-us"><?php echo get_field('location_phone_number', $post_location); ?></p>
							<p class="call-us"><?php echo get_field('location_email_address', $post_location); ?></p>
							<p class="text-orange">Opening times</p>
							<?php echo get_field('location_opening_times', $post_location); ?>
						</div>
						<div class="col-md-6">

							<?php

							$location = get_field('location_map', $post_location);

							if( !empty($location) ):
								?>

								<div class="acf-map">
									<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
								</div>

								<?php

							endif;

							?>

						</div>

						<?php

						wp_reset_postdata();
					endif;

					?>

				</div>
				<div class="row">

					<?php

					$post_form = get_sub_field('fc_office_contact_form');

					if ($post_form):
						?>
						<div class="col-12">
							<?php
							setup_postdata($post_form);
							echo do_shortcode('[contact-form-7 id="'.$post_form->ID.'" html_name="'.$post_form->post_title.'" ]');
							wp_reset_postdata();
							?>
						</div>
					<?php
					endif;

					?>
				</div>
			</div>
		</div>

	</section>


	<?php
}

function get_section_team() {

	if( get_sub_field('fc_team_format') == 'list' ):

		?>

		<section class="list section__team">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<?php

					sidebar_menu_setup();

					?>

					<div class="introduction">

						<?php
							echo get_sub_field('fc_team_introduction');
						?>

					</div>

					<nav class="bg-light">
						<ul id="menu" class="sub">
							<li><span class="menu-item first">Filter by location</span></li>
							<li>
								<a href="<?php echo strtok(get_permalink(), "?"); ?>" class=" team-ajax-filter <?php echo (empty($_GET['office']) ? 'active' : ''); ?>">All</a>
							</li>

							<?php

							$query = new WP_Query( array(
									'post_type' => 'location',
									'orderby'   => 'menu_order',
									'order'     => 'ASC'
							) );

							if ( $query->have_posts() ) : ?>
								<?php while ( $query->have_posts() ) : $query->the_post();

									if (has_post_thumbnail(get_the_ID())):
										$image = get_the_post_thumbnail_url(get_the_ID(),'full');
									endif;

									$office_slug = basename(get_permalink());
									$office_id = get_the_ID();

									?>

									<li><a href="?office=<?php echo $office_id; ?>" class=" team-ajax-filter <?php echo ($_GET['office'] == get_the_ID() ? 'active' : ''); ?>"><?php the_title(); ?></a></li>
									<?php

								endwhile;
								wp_reset_postdata();

							endif;
							?>

						</ul>
					</nav>

					<?php

					$office_id =  $_GET["office"];

					$args = array(
						'post_type'	=> 'team',
						'posts_per_page' => '-1',
						'orderby'   => 'menu_order',
						'order'     => 'ASC'
					);

					$query = new WP_Query( $args);

					if (!empty($office_id)):

						$args_query = array(
							'meta_query' => array(
								array(
									'key' => 'team_office',
									'value' => $office_id,
									'compare' => 'IN'
								)
							)
						);

						$query = new WP_Query( $args + $args_query);

					endif;


					if ( $query->have_posts() ) :

						?>

						<div class="ajax-wrapper row">

							<?php

							while ( $query->have_posts() ) : $query->the_post();

								if (has_post_thumbnail(get_the_ID())):

									$image = get_the_post_thumbnail_url(get_the_ID(),'full');

								endif;

								?>

								<div class="ajax-item col-md-6">
									<a href="<?php echo get_permalink(get_the_ID()); ?>"
										class="profile">
										<div class="image" style="background-image:url('<?= $image; ?>')"></div>
										<div class="content">
											<h4><?php echo get_the_title(get_the_ID()) ?></h4>
											<h5><span class="text-background"><?php echo get_field('team_job_title', get_the_ID()); ?></span></h5>
										</div>
									</a>
								</div>
								<?php

							endwhile;
							wp_reset_postdata();

							?>
						</div>

						<?php

					endif;

					?>

				</div>
			</div>
		</section>
		<?php

	elseif( get_sub_field('fc_team_format') == 'all' ) :


		?>

		<section class="section__team" data-aos="fade-up">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<div class="col-md-6 left-col">
						<h3 class="text-orange">Meet the team</h3>
						<div id="teamCarousel1" class="carousel slide carousel-sync" data-ride="carousel">
							<div class="carousel-inner">

								<?php
								$args = array(
									'post_type'	=> 'team',
									'posts_per_page' => '-1'
								);

								$query = new WP_Query( $args);

								$i = 0;
								if ( $query->have_posts() ) :

									while ( $query->have_posts() ) : $query->the_post();

										if (has_post_thumbnail(get_the_ID())):

											$image = get_the_post_thumbnail_url(get_the_ID(),'full');

										endif;

										?>

										<div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
											<h4><?php echo get_the_title(get_the_ID()); ?></h4>
											<h5><span><?php echo get_field('team_job_title', get_the_ID()); ?></span></h5>
											<p><?php echo the_content(get_the_ID()); ?></p>
											<p><a href="<?php echo get_permalink(get_the_ID()); ?>" class="append-arrow">View profile</a></p>
										</div>
										<?php

									$i++;
									endwhile;
									wp_reset_postdata();

								endif;

								?>

							</div>
							<a class="carousel-control-prev" href="#teamCarousel1" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#teamCarousel1" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<div class="col-md-6 right-col">
						<div id="teamCarousel2" class="carousel slide carousel-sync" data-ride="carousel">
							<div class="carousel-inner">

								<?php

								$i = 0;
								if ( $query->have_posts() ) :

									while ( $query->have_posts() ) : $query->the_post();

										if (has_post_thumbnail(get_the_ID())):

											$image = get_the_post_thumbnail_url(get_the_ID(),'full');

										endif;

										?>

										<div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
											<div class="image" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full');  ?>);"></div>
										</div>

										<?php

										$i++;
									endwhile;

									wp_reset_postdata();

								endif;

								?>

							</div>
						</div>
					</div>
				</div>
			</div>
			<script>
				var carousel1 = $('#teamCarousel1').carousel({interval: false});
				var carousel2= $('#teamCarousel2').carousel({interval: false});

				$('.carousel-sync').on('slide.bs.carousel', function(ev) {
					var dir = ev.direction == 'right' ? 'prev' : 'next';
					$('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
				});
				$('.carousel-sync').on('slid.bs.carousel', function(ev) {
					$('.carousel-sync').removeClass('sliding');
				});
			</script>
		</section>

	<?php

	else :

	// endif;

	// if( get_sub_field('fc_team_format') == 'higlight' ):


		if( have_rows('fc_team_members') || get_field('project_team_members')):

			?>
		<section class="section__team" id="meetTheTeam" data-aos="fade-up">
			<div class="<?= container_width(); ?>">
				<div class="row">
					<div class="col-md-6 left-col">
						<h3 class="text-orange">Meet the team</h3>
						<div id="teamCarousel1" class="carousel slide carousel-sync" data-ride="carousel">
							<div class="carousel-inner">
								<?php

								$i = 0;
								if (have_rows('fc_team_members')) :
									while ( have_rows('fc_team_members') ) : the_row();
										$post_team = get_sub_field('fc_team_member');
										if( $post_team ):

											setup_postdata( $post_team );

											?>

											<div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
												<h4><?php echo get_the_title($post_team); ?></h4>
												<h5><span><?php echo get_field('team_job_title', $post_team); ?></span></h5>
												<p><?php echo the_content($post_team); ?></p>
												<p><a href="<?php echo get_permalink($post_team); ?>" class="append-arrow">View profile</a></p>
											</div>

											<?php
											wp_reset_postdata();
											$i++;
										endif;
									endwhile;
								elseif (get_field('project_team_members')) :
									$posts = get_field('project_team_members');
									foreach( $posts as $post) :
										setup_postdata($post); ?>
										<div class="carousel-item <?= ($i == 0) ? 'active' : ''; ?>">
											<h4><?php echo get_the_title($post); ?></h4>
											<h5><span class="text-background"><?= get_field('team_job_title', $post); ?></span></h5>
											<p><?php echo the_content($post) ?></p>
											<p><a href="<?= get_the_permalink($post); ?>" class="append-arrow">View profile</a></p>
										</div>
										<?php
										$i++;
									endforeach;
									wp_reset_postdata();

								endif;

								?>
							</div>
							<?php if ($i >= 2) : ?>
							<a class="carousel-control-prev" href="#teamCarousel1" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#teamCarousel1" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-6 right-col">
						<div id="teamCarousel2" class="carousel slide carousel-sync" data-ride="carousel">
							<div class="carousel-inner">

								<?php

								$i = 0;
								if (have_rows('fc_team_members')) :
									while ( have_rows('fc_team_members') ) : the_row();
										$post_team = get_sub_field('fc_team_member');
										if( $post_team ):

											setup_postdata( $post_team );

											?>

											<div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
												<div class="image" style="background-image: url(<?php echo get_the_post_thumbnail_url($post_team,'full');  ?>);"></div>
											</div>

											<?php
											wp_reset_postdata();
											$i++;
										endif;
									endwhile;
								elseif (get_field('project_team_members')) :
									$posts = get_field('project_team_members');

									foreach( $posts as $post):?>
										<?php setup_postdata($post); ?>
										<div class="carousel-item <?php echo ($i == 0) ? 'active' : ''; ?>">
											<div class="image" style="background-image: url(<?php echo get_the_post_thumbnail_url($post,'full');  ?>);"></div>
										</div>
										<?php
										$i++;
									endforeach;
									wp_reset_postdata();

								endif;

								?>

							</div>
						</div>
					</div>
				</div>
			</div>
			<script>
				var carousel1 = $('#teamCarousel1').carousel({interval: false});
				var carousel2= $('#teamCarousel2').carousel({interval: false});

				$('.carousel-sync').on('slide.bs.carousel', function(ev) {
					var dir = ev.direction == 'right' ? 'prev' : 'next';
					$('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
				});
				$('.carousel-sync').on('slid.bs.carousel', function(ev) {
					$('.carousel-sync').removeClass('sliding');
				});
			</script>
		</section>


		<?php

		endif;

	endif;



}

function get_section_projects() {
	if( get_sub_field('fc_projects_blockscarousel') == 'carousel' ):

		?>

		<section id="imageCarousel" class="carousel slide carousel-sync" data-ride="carousel">
			<div class="carousel-inner">

				<?php

				if( have_rows('fc_projects_repeater') ):
					$i = 0;

					while ( have_rows('fc_projects_repeater') ) : the_row();

						$active_class = ($i == 0) ? ' active' : '';

						$post_project = get_sub_field('fc_projects_repeater_project');

						if( $post_project ):

							setup_postdata( $post_project );

							if (has_post_thumbnail($post_project)):
								$image = get_the_post_thumbnail_url($post_project,'full');

							else:

								if( have_rows('project_gallery', $post_project) ):
									$j = 0;

									while ( have_rows('project_gallery', $post_project) ) : the_row();
										if ($j == 0) :

											$image = get_sub_field('project_gallery_image', $post_project);
											$image = $image['url'];
											$j++;

										endif;

									endwhile;

								endif;

							endif;

							echo '<div class="carousel-item' . $active_class . '"><div style="background-image: url(' . $image . ')"></div></div>';

						endif;

						wp_reset_postdata();
						$i++;

					endwhile;

				endif;

				?>

			</div>
			<a class="carousel-control-prev" href="#imageCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#imageCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</section>
		<section id="imageCarouselCaption" class="carousel-fade slide carousel-sync" data-ride="carousel">
			<div class="carousel-inner">

				<?php

				if( have_rows('fc_projects_repeater') ):

					$i = 0;

					while ( have_rows('fc_projects_repeater') ) : the_row();

						$active_class = ($i == 0) ? ' active' : '';
						$post_project = get_sub_field('fc_projects_repeater_project');

						if( $post_project ):

							setup_postdata( $post_project );

							echo '<div class="carousel-item' . $active_class . '">
							<div class="view-details"><h4>' . get_field('project_sub_title', $post_project) .'</h4>
							<h5>' . get_field('project_location', $post_project) .'</h5></div>
							<div class="view-project"><a href="' . get_permalink($post_project) . '" class="append-arrow">View Project</a></div>
							</div>';

						endif;

						wp_reset_postdata();
						$i++;

					endwhile;

				endif;
				?>
			</div>
		</section>
		<script>
			var carousel1 = $('#imageCarousel').carousel({interval: false});
			var carousel2= $('#imageCarouselCaption').carousel({interval: false});

			$('.carousel-sync').on('slide.bs.carousel', function(ev) {
				var dir = ev.direction == 'right' ? 'prev' : 'next';
				$('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
			});
			$('.carousel-sync').on('slid.bs.carousel', function(ev) {
				$('.carousel-sync').removeClass('sliding');
			});
		</script>

		<?php

	else:

		?>

		<section id="imageBlockCarousel">
			<?php

			if(get_sub_field('fc_projects_title', $page_id)):

				?>

				<div class="<?= container_width(); ?> title">
					<div class="row">
						<div class="col">
							<h3><?php echo get_sub_field('fc_projects_title', $page_id); ?></h3>

							<?php

								if (get_sub_field('fc_projects_link', $page_id)) :

									?>

										<a href="<?php echo get_sub_field('fc_projects_link', $page_id); ?>" class="append-arrow"><?php echo (get_sub_field('fc_projects_link_text', $page_id)) ? get_sub_field('fc_projects_link_text', $page_id) : 'View more'; ?></a>

									<?php

								endif;

							?>

							</div>
						</div>
					</div>

					<?php

				endif;

				?>

				<div class="<?= container_width(); ?>">
					<div class="row">
						<?php

						$count = count(get_sub_field('fc_projects_repeater', $page_id));
						$grid = array('col-md-12', 'col-md-6', 'col-md-6');

						if ($count == 1) :

							$grid = array('col-md-12');

						elseif ($count == 2) :

							$grid = array('col-md-6', 'col-md-6');

						elseif ($count == 3) :

							$grid = array('col-md-12', 'col-md-6', 'col-md-6');

						endif;

						if( have_rows('fc_projects_repeater', get_the_id()) ) :

							$i = 0;

							while ( have_rows('fc_projects_repeater', get_the_ID()) ) : the_row();

								$post_project = get_sub_field('fc_projects_repeater_project');

								if( $post_project ):

									setup_postdata( $post_project );

									if (has_post_thumbnail($post_project)):
										$image = get_the_post_thumbnail_url($post_project,'full');

									else:

										if ( have_rows('project_gallery', $post_project) ):
											$j = 0;

											while ( have_rows('project_gallery', $post_project) ) : the_row();

												if ($j == 0) :

													$image = get_sub_field('project_gallery_image', $post_project);
													$image = $image['url'];
													$j++;

												endif;

											endwhile;

										endif;

									endif;

									?>

									<div class="<?php echo $grid[$i]; ?> no-pad" data-aos="fade-up">
										<a href="<?php echo get_permalink($post_project); ?>"
											class="project">
											<div class="content">
												<h4><?php echo get_the_title($post_project) ?></h4>
												<h5><?php echo get_field('project_sub_title', $post_project); ?></h5>												
											</div>
											<p class="view-project-link"><span class="append-arrow">View project</span></p>
											<div class="overlay"></div>
											<span class="lazy-loader"></span>
											<div class="image full-col lazy" data-src="<?= $image; ?>);"></div>
										</a>
									</div>

									<?php

									wp_reset_postdata();
									$i++;

								endif;

							endwhile;

						endif;

						?>

					</div>
				</div>
			</section>

			<?php


		endif;


	}


function get_section_related_projects() {

		// from projects CPT
	if (get_field('project_related_projects', $page_id)) :
		$post_related_projects = get_field('project_related_projects', $page_id);
	endif;

	if( $post_related_projects ):
		$count = count($post_related_projects);
		$i = 0;

		?>

		<section id="relatedProjects">
			<div class="<?= container_width(); ?> title-block">
				<div class="row">
					<div class="col">
						<h3>Related projects</h3>

						<?php

							if (get_sub_field('fc_projects_link', $page_id)) :

								?>

									<a href="<?php echo get_sub_field('fc_projects_link', $page_id); ?>" class="append-arrow"><?php echo (get_sub_field('fc_projects_link_text', $page_id)) ? get_sub_field('fc_projects_link_text', $page_id) : 'View more'; ?></a>

								<?php

							endif;

						?>

					</div>
				</div>
			</div>
			<div class="<?= container_width(); ?> no-pad">
				<div class="row">
					<?php

					foreach( $post_related_projects as $post_related_project):
						setup_postdata($post_related_project);

						if (has_post_thumbnail($post_related_project)):
							$image = get_the_post_thumbnail_url($post_related_project,'full');

						else:

							if( have_rows('project_gallery', $post_related_project) ):
								$j = 0;

								while ( have_rows('project_gallery', $post_related_project) ) : the_row();
									if ($j == 0) :

										$image = get_sub_field('project_gallery_image', $post_related_project);
										$image = $image['url'];
										$j++;
									endif;

								endwhile;

							endif;

						endif;

						?>

						<div class="col-md-6 no-pad">
							<a href="<?php echo get_permalink($post_related_project); ?>" class="project">
								<div class="content">
									<h4><?php echo get_the_title($post_related_project) ?></h4>
									<h5><?php echo get_field('project_sub_title', $post_related_project); ?></h5>
								</div>
								<p class="view-project-link"><span class="append-arrow">View project</span></p>
								<div class="overlay"></div>
								<div class="image half lazy" data-src="<?= $image; ?>"></div>
							</a>
						</div>
						<?php

					endforeach;
					wp_reset_postdata();

					?>

				</div>
			</div>
		</section>

	<?php

	endif;

}


function get_section_images() {
		?>

		<section id="imageBlockCarousel">


				<div class="<?= container_width(); ?>">
					<div class="row">
						<?php

						$count = count(get_sub_field('fc_projects_repeater_images', $page_id));
						$grid = array('col-md-12', 'col-md-6', 'col-md-6');

						if ($count == 1) :

							$grid = array('col-md-12');

						elseif ($count == 2) :

							$grid = array('col-md-6', 'col-md-6');

						elseif ($count == 3) :

							$grid = array('col-md-12', 'col-md-6', 'col-md-6');

						endif;

						if( have_rows('fc_projects_repeater_images', $page_id) ) :

							$i = 0;

							while ( have_rows('fc_projects_repeater_images', $page_id) ) : the_row();

								$rows[] = get_sub_field('fc_projects_repeater_images_image');

									?>

									<div class="<?php echo $grid[$i]; ?>" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image full-col lazy" data-src="<?= $rows[$i]['url']; ?>"></div>
									</div>

									<?php

									wp_reset_postdata();
									$i++;



							endwhile;

						endif;

						?>

					</div>
				</div>
			</section>

			<?php



	}

?>