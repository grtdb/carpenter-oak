<?php

function load_more_scripts() {

	global $wp_query;

	// register our main script but do not enqueue it yet
	wp_register_script( 'loadmore', get_stylesheet_directory_uri() . '/assets/js/src/jquery/loadmore.js', [ 'jquery' ], '3.0' );

	// Localise script to inject page data
	wp_localize_script( 'loadmore', 'loadmore_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts'        => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'max_page'     => $wp_query->max_num_pages
	] );

	wp_enqueue_script( 'loadmore' );
}

add_action( 'wp_enqueue_scripts', 'load_more_scripts' );

function loadmore_ajax_handler() {

	// prepare our arguments for the query
	$args                = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged']       = $_POST['page'] + 1; // we need next page to be loaded
	$args['template']    = $_POST['template'];
	$args['post_status'] = 'publish';

	query_posts( $args );

	if ( empty( $args['template'] ) ) {
		echo 'No "template" var passed in loadmore part get';
	} else {
		DbHelper::get_part( $args['template'] );
	}
	die;
}

add_action( 'wp_ajax_loadmore', 'loadmore_ajax_handler' );
add_action( 'wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler' );