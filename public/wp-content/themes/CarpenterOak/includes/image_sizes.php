<?php /* Custom Image Sizes */

add_image_size( 'news-half-post', 476, 476, true );
add_image_size( 'news-full-post', 952, 663, true );
add_image_size( 'grid-half-half', 954, 476, true );
add_image_size( 'grid-half-full', 954, 954, true );
add_image_size( '4-col', 415, 275, true );
