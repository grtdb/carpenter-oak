<?php

class CallToActionsPostType extends dbCustomPostType {

	public $post_type = 'call-to-action';
	public $slug = 'call-to-action';

	public function __construct() {
		$this->post_type = 'call-to-action';

		$this->name = 'Call to Actions';
		$this->singular_name = 'Call to Action';

		$this->is_public = true;

		$this->args = array(
			'supports' => array( 'title', 'editor', 'page-attributes' ),
			'menu_position' => 22,
			'menu_icon' => 'dashicons-pressthis',
			'publicly_queryable'  => false
		);

		//$this->link_meta_box = true;
		$this->featured_image_bottom = true;

		parent::__construct();

		//add_image_size('cta_thumb', 240, 0);
	}
}

new CallToActionsPostType();


// Add column titles to wp-admin for team custom post type
function cta_columns($columns){
		$columns = array(
				'cb'	 				=> '<input type="checkbox" />',
				'title'					=> 'Title',
				'message' 				=> 'Message',
				'linked_pages' 			=> 'Linked Pages',
		);
		return $columns;
	}

add_filter("manage_edit-call-to-action_columns", "cta_columns");


// Add data to newly created columns in wp-admin for team custom post type
function cta_custom_columns($column){

	global $post;

	if($column == 'message'){
		echo get_field('cta_message');
	}elseif($column == 'page_link'){
		$link = get_field('cta_page_link');
		if( $link ): 
			echo '<a href="' . get_edit_post_link($link->ID) .'">' . $link->post_title . '</a>';
		endif;
	}elseif($column == 'linked_pages'){
		$posts = get_field('cta_add_to_pages');

		if( $posts ): 
			foreach( $posts as $post): 
				setup_postdata($post);

				echo '<a href="' . get_edit_post_link() .'">' . get_the_title() . '</a>';

				// Add comma after all project links except last
				if (next($posts)) {
					echo ', ';
				}

			endforeach;
			wp_reset_postdata();
		endif;
	}
}

add_action("manage_posts_custom_column", "cta_custom_columns");