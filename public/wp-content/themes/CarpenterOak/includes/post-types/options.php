<?php 

function change_title_placeholder_text( $title ){
     $screen = get_current_screen();
  
     if  ( 'team' == $screen->post_type ) {
          $title = 'Enter team member\'s full name here';
     } else if ( 'project' == $screen->post_type ) {
          $title = 'Enter project title here';
     } else if ( 'testimonial' == $screen->post_type ) {
          $title = 'Enter name here';
     } else if ( 'location' == $screen->post_type ) {
          $title = 'Enter location name here';
     } else if ( 'call-to-action' == $screen->post_type ) {
          $title = 'Enter CTA title here';
     }
  
     return $title;
}
  
add_filter( 'enter_title_here', 'change_title_placeholder_text' );