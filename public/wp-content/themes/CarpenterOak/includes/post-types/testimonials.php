<?php
class TestimonialsPostType extends dbCustomPostType {
	public function __construct() {
		$this->post_type = 'testimonial';

		$this->name = 'Testimonials';
		$this->singular_name = 'Testimonial';

		$this->is_public = false;

		$this->args = array(
			'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
			'menu_position' => 20,
			'menu_icon' => 'dashicons-format-quote'
		);

		//$this->link_meta_box = true;
		$this->featured_image_bottom = true;

		parent::__construct();

		//add_image_size('cta_thumb', 240, 0);
	}
}

new TestimonialsPostType();


// Add column titles to wp-admin for team custom post type
function testimonial_columns($columns){
		$columns = array(
				'cb'	 				=> '<input type="checkbox" />',
				'title'					=> 'Name',
				'testimonial' 			=> 'Testimonial',
				'project_link' 			=> 'Project Link',
		);
		return $columns;
	}

add_filter("manage_edit-testimonial_columns", "testimonial_columns");


// Add data to newly created columns in wp-admin for team custom post type
function testimonial_custom_columns($column){

	global $post;

	if($column == 'testimonial'){
		echo get_field('testimonial_content');
	}elseif($column == 'project_link'){
		$posts = get_field('testimonial_project_links');

		if( $posts ): 
			foreach( $posts as $post): 
				setup_postdata($post);

				echo '<a href="' . get_edit_post_link() .'">' . get_the_title() . '</a>';

				// Add comma after all project links except last
				if (next($posts)) {
					echo ', ';
				}

			endforeach;
			wp_reset_postdata();
		endif;
	}
}

add_action("manage_posts_custom_column", "testimonial_custom_columns");