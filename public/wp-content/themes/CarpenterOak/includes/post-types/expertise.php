<?php

class ExpertisePostType extends dbCustomPostType {

	public $post_type = 'expertise';
	public $slug = 'expertise';

	public function __construct() {
		$this->post_type = 'expertise';

		$this->name = 'Expertise';
		$this->singular_name = 'Expertise';

		$this->is_public = true;

		$this->args = array(
			'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
			'menu_position' => 22,
			'menu_icon' => 'dashicons-admin-customizer',
			'show_in_nav_menus' => true
		);

		//$this->link_meta_box = true;
		$this->featured_image_bottom = true;

		parent::__construct();

		//add_image_size('cta_thumb', 240, 0);
	}
}

// new ExpertisePostType();
