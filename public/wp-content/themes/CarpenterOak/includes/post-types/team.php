<?php

class TeamPostType extends dbCustomPostType {

	public $post_type = 'team';
	public $slug = 'team';

	public function __construct() {
		$this->post_type = 'team';

		$this->name = 'Team';
		$this->singular_name = 'Team member';

		$this->is_public = true;

		$this->args = array(
			'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
			'menu_position' => 20,
			'menu_icon' => 'dashicons-admin-users'
		);

		//$this->link_meta_box = true;
		$this->featured_image_bottom = true;

		parent::__construct();

		//add_image_size('cta_thumb', 240, 0);
	}
}

new TeamPostType();


// Add column titles to wp-admin for team custom post type
function team_columns($columns){
		$columns = array(
				'cb'	 				=> '<input type="checkbox" />',
				'title'					=> 'Team Member',
				'job_title' 			=> 'Job Title',
		);
		return $columns;
	}

add_filter("manage_edit-team_columns", "team_columns");


// Add data to newly created columns in wp-admin for team custom post type
function team_custom_columns($column){

		global $post;

		if($column == 'job_title'){
			echo get_field('team_job_title');
		}
	}

add_action("manage_posts_custom_column", "team_custom_columns");