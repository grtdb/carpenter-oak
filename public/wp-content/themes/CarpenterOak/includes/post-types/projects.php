<?php



// Register Building Type Taxonomy
function buildingType_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Building Type', 'Taxonomy General Name', 'carpenter_oak' ),
		'singular_name'              => _x( 'Building Type', 'Taxonomy Singular Name', 'carpenter_oak' ),
		'menu_name'                  => __( 'Building Type', 'carpenter_oak' ),
		'all_items'                  => __( 'All Building Type', 'carpenter_oak' ),
		'parent_item'                => __( 'Parent Building Type', 'carpenter_oak' ),
		'parent_item_colon'          => __( 'Parent Building Type:', 'carpenter_oak' ),
		'new_item_name'              => __( 'New Building Type Name', 'carpenter_oak' ),
		'add_new_item'               => __( 'Add New Building Type', 'carpenter_oak' ),
		'edit_item'                  => __( 'Edit Building Type', 'carpenter_oak' ),
		'update_item'                => __( 'Update Building Type', 'carpenter_oak' ),
		'view_item'                  => __( 'View Building Type', 'carpenter_oak' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'carpenter_oak' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'carpenter_oak' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'carpenter_oak' ),
		'popular_items'              => __( 'Popular Building Type', 'carpenter_oak' ),
		'search_items'               => __( 'Search Building Type', 'carpenter_oak' ),
		'not_found'                  => __( 'Not Found', 'carpenter_oak' ),
		'no_terms'                   => __( 'No items', 'carpenter_oak' ),
		'items_list'                 => __( 'Building Type list', 'carpenter_oak' ),
		'items_list_navigation'      => __( 'Building Type list navigation', 'carpenter_oak' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'show_in_quick_edit'         => false,
	    'meta_box_cb'                => false,
	);
	register_taxonomy( 'building-type', array( 'project' ), $args );

}
add_action( 'init', 'buildingType_taxonomy', 0 );


// Register Locations Taxonomy
function locations_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Locations', 'Taxonomy General Name', 'carpenter_oak' ),
		'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'carpenter_oak' ),
		'menu_name'                  => __( 'Locations', 'carpenter_oak' ),
		'all_items'                  => __( 'All Locations', 'carpenter_oak' ),
		'parent_item'                => __( 'Parent Location', 'carpenter_oak' ),
		'parent_item_colon'          => __( 'Parent Location:', 'carpenter_oak' ),
		'new_item_name'              => __( 'New Location Name', 'carpenter_oak' ),
		'add_new_item'               => __( 'Add New Location', 'carpenter_oak' ),
		'edit_item'                  => __( 'Edit Location', 'carpenter_oak' ),
		'update_item'                => __( 'Update Location', 'carpenter_oak' ),
		'view_item'                  => __( 'View Location', 'carpenter_oak' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'carpenter_oak' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'carpenter_oak' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'carpenter_oak' ),
		'popular_items'              => __( 'Popular Locations', 'carpenter_oak' ),
		'search_items'               => __( 'Search Locations', 'carpenter_oak' ),
		'not_found'                  => __( 'Not Found', 'carpenter_oak' ),
		'no_terms'                   => __( 'No items', 'carpenter_oak' ),
		'items_list'                 => __( 'Locations list', 'carpenter_oak' ),
		'items_list_navigation'      => __( 'Locations list navigation', 'carpenter_oak' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_tagcloud'              => false,
		'show_in_quick_edit'         => false,
	    'meta_box_cb'                => false,
	);
	register_taxonomy( 'locations', array( 'project' ), $args );

}
add_action( 'init', 'locations_taxonomy', 0 );


// Register Project Category Taxonomy
function project_categories_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'carpenter_oak' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'carpenter_oak' ),
		'menu_name'                  => __( 'Categories', 'carpenter_oak' ),
		'all_items'                  => __( 'All Categories', 'carpenter_oak' ),
		'parent_item'                => __( 'Parent Category', 'carpenter_oak' ),
		'parent_item_colon'          => __( 'Parent Category:', 'carpenter_oak' ),
		'new_item_name'              => __( 'New Category Name', 'carpenter_oak' ),
		'add_new_item'               => __( 'Add New Category', 'carpenter_oak' ),
		'edit_item'                  => __( 'Edit Category', 'carpenter_oak' ),
		'update_item'                => __( 'Update Category', 'carpenter_oak' ),
		'view_item'                  => __( 'View Category', 'carpenter_oak' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'carpenter_oak' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'carpenter_oak' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'carpenter_oak' ),
		'popular_items'              => __( 'Popular Categories', 'carpenter_oak' ),
		'search_items'               => __( 'Search Categories', 'carpenter_oak' ),
		'not_found'                  => __( 'Not Found', 'carpenter_oak' ),
		'no_terms'                   => __( 'No items', 'carpenter_oak' ),
		'items_list'                 => __( 'Categories list', 'carpenter_oak' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'carpenter_oak' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'show_tagcloud'              => false,
		'show_in_quick_edit'         => false,
	    'meta_box_cb'                => false,
	);
	register_taxonomy( 'project-category', array( 'project' ), $args );

}
add_action( 'init', 'project_categories_taxonomy', 0 );


// Register Architects Taxonomy
function architects_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Architects', 'Taxonomy General Name', 'carpenter_oak' ),
		'singular_name'              => _x( 'Architect', 'Taxonomy Singular Name', 'carpenter_oak' ),
		'menu_name'                  => __( 'Architects', 'carpenter_oak' ),
		'all_items'                  => __( 'All Architects', 'carpenter_oak' ),
		'parent_item'                => __( 'Parent Architect', 'carpenter_oak' ),
		'parent_item_colon'          => __( 'Parent Architect:', 'carpenter_oak' ),
		'new_item_name'              => __( 'New Architect Name', 'carpenter_oak' ),
		'add_new_item'               => __( 'Add New Architect', 'carpenter_oak' ),
		'edit_item'                  => __( 'Edit Architect', 'carpenter_oak' ),
		'update_item'                => __( 'Update Architect', 'carpenter_oak' ),
		'view_item'                  => __( 'View Architect', 'carpenter_oak' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'carpenter_oak' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'carpenter_oak' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'carpenter_oak' ),
		'popular_items'              => __( 'Popular Architects', 'carpenter_oak' ),
		'search_items'               => __( 'Search Architects', 'carpenter_oak' ),
		'not_found'                  => __( 'Not Found', 'carpenter_oak' ),
		'no_terms'                   => __( 'No items', 'carpenter_oak' ),
		'items_list'                 => __( 'Architects list', 'carpenter_oak' ),
		'items_list_navigation'      => __( 'Architects list navigation', 'carpenter_oak' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_tagcloud'              => false,
		'show_in_quick_edit'         => false,
	    'meta_box_cb'                => false,
	);
	register_taxonomy( 'architects', array( 'project' ), $args );

}
add_action( 'init', 'architects_taxonomy', 0 );


// Register Budgets Taxonomy
function budgets_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Budgets', 'Taxonomy General Name', 'carpenter_oak' ),
		'singular_name'              => _x( 'Budget', 'Taxonomy Singular Name', 'carpenter_oak' ),
		'menu_name'                  => __( 'Budgets', 'carpenter_oak' ),
		'all_items'                  => __( 'All Budgets', 'carpenter_oak' ),
		'parent_item'                => __( 'Parent Budget', 'carpenter_oak' ),
		'parent_item_colon'          => __( 'Parent Budget:', 'carpenter_oak' ),
		'new_item_name'              => __( 'New Budget Name', 'carpenter_oak' ),
		'add_new_item'               => __( 'Add New Budget', 'carpenter_oak' ),
		'edit_item'                  => __( 'Edit Budget', 'carpenter_oak' ),
		'update_item'                => __( 'Update Budget', 'carpenter_oak' ),
		'view_item'                  => __( 'View Budget', 'carpenter_oak' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'carpenter_oak' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'carpenter_oak' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'carpenter_oak' ),
		'popular_items'              => __( 'Popular Budgets', 'carpenter_oak' ),
		'search_items'               => __( 'Search Budgets', 'carpenter_oak' ),
		'not_found'                  => __( 'Not Found', 'carpenter_oak' ),
		'no_terms'                   => __( 'No items', 'carpenter_oak' ),
		'items_list'                 => __( 'Budgets list', 'carpenter_oak' ),
		'items_list_navigation'      => __( 'Budgets list navigation', 'carpenter_oak' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_tagcloud'              => false,
		'show_in_quick_edit'         => false,
	    'meta_box_cb'                => false,
	);
	register_taxonomy( 'budgets', array( 'project' ), $args );

}
add_action( 'init', 'budgets_taxonomy', 0 );


// Register Styles Taxonomy
function styles_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Styles', 'Taxonomy General Name', 'carpenter_oak' ),
		'singular_name'              => _x( 'Style', 'Taxonomy Singular Name', 'carpenter_oak' ),
		'menu_name'                  => __( 'Styles', 'carpenter_oak' ),
		'all_items'                  => __( 'All Styles', 'carpenter_oak' ),
		'parent_item'                => __( 'Parent Style', 'carpenter_oak' ),
		'parent_item_colon'          => __( 'Parent Style:', 'carpenter_oak' ),
		'new_item_name'              => __( 'New Style Name', 'carpenter_oak' ),
		'add_new_item'               => __( 'Add New Style', 'carpenter_oak' ),
		'edit_item'                  => __( 'Edit Style', 'carpenter_oak' ),
		'update_item'                => __( 'Update Style', 'carpenter_oak' ),
		'view_item'                  => __( 'View Style', 'carpenter_oak' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'carpenter_oak' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'carpenter_oak' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'carpenter_oak' ),
		'popular_items'              => __( 'Popular Styles', 'carpenter_oak' ),
		'search_items'               => __( 'Search Styles', 'carpenter_oak' ),
		'not_found'                  => __( 'Not Found', 'carpenter_oak' ),
		'no_terms'                   => __( 'No items', 'carpenter_oak' ),
		'items_list'                 => __( 'Styles list', 'carpenter_oak' ),
		'items_list_navigation'      => __( 'Styles list navigation', 'carpenter_oak' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'show_tagcloud'              => false,
		'show_in_quick_edit'         => false,
	    'meta_box_cb'                => false,
	);
	register_taxonomy( 'styles', array( 'project' ), $args );

}
add_action( 'init', 'styles_taxonomy', 0 );


class ProjectsPostType extends dbCustomPostType {

	public $post_type = 'project';
	public $slug = 'projects';

	public function __construct() {
		$this->post_type = 'project';

		$this->name = 'Projects';
		$this->singular_name = 'Project';

		$this->is_public = true;

		$this->args = array(
			'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
			'menu_position' => 20,
			'menu_icon' => 'dashicons-hammer'
		);

		//$this->link_meta_box = true;
		$this->featured_image_bottom = true;

		parent::__construct();

		//add_image_size('cta_thumb', 240, 0);
	}
}

new ProjectsPostType();




// Add column titles to wp-admin for team custom post type
function project_columns($columns){
		$columns = array(
				'cb'	 				=> '<input type="checkbox" />',
				'title'					=> 'Name',
				'location' 				=> 'Location',
				'project_categories' 	=> 'Categories',
				'building-type'         => 'Building Type'
		);
		return $columns;
	}

add_filter("manage_edit-project_columns", "project_columns");


// Add data to newly created columns in wp-admin for team custom post type
function project_custom_columns($column){

	global $post;

	if($column == 'location'){
		$term = get_field('project_grouped_location');
		if( $term ): 
			echo '<a href="' . get_edit_term_link($term->term_id) .'">' . $term->name . '</a>';
		endif;
		
	}elseif($column == 'project_categories'){
		$terms = get_field('project_categories');
		if( $terms ): 
			foreach( $terms as $term): 

				echo '<a href="' . get_edit_term_link($term->term_id) .'">' . $term->name . '</a>';

				// Add comma after all project links except last
				if (next($terms)) {
					echo ', ';
				}

			endforeach;
		endif;
	}elseif($column == 'building-type'){
		$term = get_field('project_building_tpye');
		if( $term ):
			echo '<a href="' . get_edit_term_link($term->term_id) .'">' . $term->name . '</a>';
		endif;
	}
}

add_action("manage_posts_custom_column", "project_custom_columns");


