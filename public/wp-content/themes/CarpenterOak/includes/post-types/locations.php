<?php

class LocationsPostType extends dbCustomPostType {

	public $post_type = 'location';
	public $slug = 'locations';

	public function __construct() {
		$this->post_type = 'location';

		$this->name = 'Locations';
		$this->singular_name = 'Location';

		$this->is_public = true;

		$this->args = array(
			'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
			'menu_position' => 20,
			'menu_icon' => 'dashicons-location'
		);

		//$this->link_meta_box = true;
		$this->featured_image_bottom = true;

		parent::__construct();

		//add_image_size('cta_thumb', 240, 0);
	}
}

new LocationsPostType();


// Add column titles to wp-admin for team custom post type
function location_columns($columns){
		$columns = array(
				'cb'	 				=> '<input type="checkbox" />',
				'title'					=> 'Location',
				'address' 				=> 'Address',
		);
		return $columns;
	}

add_filter("manage_edit-location_columns", "location_columns");


// Add data to newly created columns in wp-admin for team custom post type
function location_custom_columns($column){

	global $post;

	if($column == 'address'){
		echo get_field('location_address');
	}
}

add_action("manage_posts_custom_column", "location_custom_columns");