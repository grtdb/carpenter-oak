<?php 

function get_sections() {
$page_id = get_the_ID();


// check if the flexible content field has rows of data
if( have_rows('section') ):

     // loop through the rows of data
	while ( have_rows('section') ) : the_row();

		if( get_row_layout() == 'fc_text' ): 

			get_section_text();

		elseif( get_row_layout() == 'fc_faq' ): 

			get_section_faq();
			
		elseif( get_row_layout() == 'fc_office' ): 
			
			get_section_office();

		elseif( get_row_layout() == 'fc_team' ):

			get_section_team();

		elseif( get_row_layout() == 'fc_video' ):

			get_section_video();

		elseif( get_row_layout() == 'fc_testimonial' ):

			get_section_testimonial();
			
		elseif( get_row_layout() == 'fc_project_start' ):

			get_section_project_start();

		elseif( get_row_layout() == 'fc_building_type' ):
			
			get_section_building_type();

		elseif( get_row_layout() == 'fc_projects' ):

			get_section_projects();

		elseif( get_row_layout() == 'fc_images' ):

			get_section_images();

		elseif( get_row_layout() == 'fc_cta' ):

			get_section_cta();

		elseif( get_row_layout() == 'fc_inline_cta' ):

			get_inline_section_cta();

		endif;

	endwhile;

else :

    // no layouts found

	endif;
}
	?>