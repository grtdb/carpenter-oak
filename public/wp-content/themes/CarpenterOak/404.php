<?php
global $wp_query;
$post = get_page_by_path('404-page-not-found',OBJECT,'page');;
$wp_query->queried_object = $post;
$wp_query->is_single = true;
$wp_query->is_404 = false;
$wp_query->queried_object_id = $post->ID;
$wp_query->post_count = 1;
$wp_query->current_post=-1;
$wp_query->posts = array($post);

get_header();
the_post();
$page_id = get_the_ID();

get_hero();
get_sections();
get_footer();