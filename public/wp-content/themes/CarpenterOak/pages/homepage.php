<?php /* Template Name: Homepage */
	get_header();
	the_post(); 
	$page_id = get_the_ID();


	if( have_rows( 'projects_cta', get_the_id() ) ):
		while ( have_rows( 'projects_cta', get_the_ID() ) ) : the_row();
			$desktop_image = get_sub_field('desktop_image');
			$mobile_image = get_sub_field('mobile_image');
			$link = get_sub_field('project_cta_link');
		endwhile;
	endif; 


	if (get_field('homepage_gallery_option') == 'video') :

		$post_video_source = get_field('homepage_video_source', $page_id);
		$post_video_source_url = get_field('homepage_video_embed_url', $page_id);

		if ($post_video_source == 'vimeo') :
			
			?>

			<section class="section__video">
				<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/<?php echo $post_video_source_url; ?>?title=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
			</section>

			<?php

		else :

			?>

			<section class="section__video">
				<div style="padding:56.25% 0 0 0;position:relative;"><iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $post_video_source_url; ?>?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>
			</section>

			<?php

		endif;

	else :

		?>

		<section id="imageCarousel" class="carousel slide carousel-sync" data-ride="carousel">
			<div class="homeCarouselOverlay d-none d-lg-block">
				<div class="container">
					<p class="h1">I Am... </p>
					<div class="row">
						<div class="col-md-4">
							<a href="/architects/" class="btn append-arrow">An Architect</a>
						</div>
						<div class="col-md-4">
							<a href="/selfbuilder/" class="btn append-arrow">A Self Builder</a>
						</div>
						<div class="col-md-4">
							<a href="/contractor/" class="btn append-arrow">A Contractor</a>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-inner main-inner">

				<?php
				if( have_rows('homepage_project_gallery') ):

					$count = count(get_field('homepage_project_gallery'));
					$i = 0;

					while ( have_rows('homepage_project_gallery') ) : the_row();

						$active_class = ($i == 0) ? ' active' : '';

						$post_object = get_sub_field('homepage_gallery_project');
						if( $post_object ): 

							$post = $post_object;
							setup_postdata( $post );

							if (has_post_thumbnail(get_the_ID())):
								$project_image = get_the_post_thumbnail_url(get_the_ID(),'full');

							else:

								if( have_rows('project_gallery', get_the_ID()) ):
									$j = 0;

									while ( have_rows('project_gallery', get_the_ID()) ) : the_row();
										if ($j == 0) {

											$project_image = get_sub_field('project_gallery_image', get_the_ID());
											$project_image = $project_image['url'];
											$j++;
										}

									endwhile;

								endif;

							endif;


							if ($i == 0 ) {
								echo '<div class="carousel-item' . $active_class . '" data-interval="7500"> <div style="background-image: url(' . $project_image . ')"></div></div>';
							} elseif ( $i == ( $count - 1 ) ){
								echo '<div class="carousel-item last' . $active_class . '" data-interval="7500"><div style="background-image: url(' . $project_image . '"></div></div>';
							} else {
								echo '<div class="carousel-item' . $active_class . '" data-interval="7500"> <div style="background-image: url(' . $project_image . '"></div></div>';
							}

						endif;
						wp_reset_postdata();
						$i++;
					endwhile;

				else :

			    // no rows found

				endif;
				?>
			</div>
			<a class="carousel-control-prev" href="#imageCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#imageCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
			<div id="imageCarouselCaption" class="carousel-fade slide carousel-sync" data-ride="carousel">
				<div class="carousel-inner">
					<?php
					if( have_rows('homepage_project_gallery') ):
						$i = 0;

						while ( have_rows('homepage_project_gallery') ) : the_row();

							$active_class = ($i == 0) ? ' active' : '';

							$post_object = get_sub_field('homepage_gallery_project');
							if( $post_object ): 

								$post = $post_object;
								setup_postdata( $post ); 
								// print_r($post);

								echo '<div class="carousel-item' . $active_class . '">
								<div class="view-details"><h4>' . get_field('project_sub_title', get_the_ID()) .'</h4>
								<h5>' . get_field('project_location', get_the_ID()) .'</h5></div>
								<div class="view-project"><a href="' . get_permalink() . '" class="append-arrow">View Project</a></div>
								</div>';
							endif;
							wp_reset_postdata();
							$i++;
						endwhile;

					else :

					// no rows found

					endif;
					?>
				</div>
				</div>
		</section>

		<div class="homeCarouselOverlay d-block d-lg-none">
				<div class="container">
					<p class="h1">I Am... </p>
					<div class="row">
						<div class="col-md-4">
							<a href="/architects/" class="btn append-arrow">An Architect</a>
						</div>
						<div class="col-md-4">
							<a href="/selfbuilder/" class="btn append-arrow">A Self Builder</a>
						</div>
						<div class="col-md-4">
							<a href="/contractor/" class="btn append-arrow">A Contractor</a>
						</div>
					</div>
				</div>
			</div>
		
		<script>
			
			$('.carousel-sync').on('slide.bs.carousel', function(ev) {
				var dir = ev.direction == 'right' ? 'prev' : 'next';
				$('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
			});
			$('.carousel-sync').on('slid.bs.carousel', function(ev) {
				$('.carousel-sync').removeClass('sliding');
			});
		</script>

		<?php

	endif;

	?>



<?php $text_block = get_sub_field('fc_text_block'); ?>

<section class="section__text">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-lg-6">
				<div class="sub-content introduction">
					<h1 class="text-orange"><?= get_field('homepage_introduction_title'); ?></h1>
					<p><?= get_field('homepage_introduction_content'); ?></p>
				</div>
			</div>
			<div class="col-12 col-lg-6 sub-content logo">
				<div class="w-100">
					<a href="/building-type/residential-projects/" class="btn append-arrow" id="home-cta-residentail-btn">Residential Projects</a>
				</div>
				<div class="w-100">
					<a href="/building-type/commercial-projects/" class="btn append-arrow" id="home-cta-commercial-btn">Commercial Projects</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="imageBlockCarousel">
		<div class="container-fluid">
			<div class="row">
				<?php
				$count       = count( get_field('projects_block') );
				$grid        = [ 'col-md-6', 'col-md-6', 'col-md-6', 'col-md-12', 'col-md-6', 'col-md-6', 'col-md-12', 'col-md-12', 'col-md-12' ];
				$blockHeight = [ 'full-col', 'half-row', 'half-row', 'full-col', 'full-col', 'full-col', 'full-col', 'full-col', 'full-col', 'full-col' ];
				$imageSizes  = [ 'grid-half-full', 'grid-half-half ', 'grid-half-half ', 'grid-full ', 'grid-half-full ', 'grid-half-full ', 'grid-full ', 'grid-half-half ', 'grid-half-full ', 'grid-half-full ' ];

				$args = [
					'post_type'      => 'post',
					'posts_per_page' => '2'
				];

				$query = new WP_Query( $args );

				if ( $query->have_posts() ) {

					while ( $query->have_posts() ) {
						$query->the_post();
						if (has_post_thumbnail(get_the_ID())):
							$post_image = get_the_post_thumbnail_url(get_the_ID(),'full');
						endif;

						$post_data[] = [
							'title' => get_the_title(),
							'image_url' => $post_image,
							'post_date' => get_the_date(),
							'post_link' => get_permalink(get_the_ID())
						];
					}


					wp_reset_postdata();
				}


				if( have_rows( 'projects_block', get_the_id() ) ):
					$i = 0;

					while ( have_rows( 'projects_block', get_the_ID() ) ) : the_row();

						$k = $i-1;

						$post_object = get_sub_field('projects_block_project');
						if( $post_object ): 

							$post = $post_object;
							setup_postdata( $post );




							//determine project image
							if (has_post_thumbnail(get_the_ID())):
								if ( $imageSizes[$i] == 'grid-half-half ' ) {
									$project_image = get_the_post_thumbnail_url(get_the_ID(), 'grid-half-half');
								} else {
									$project_image = get_the_post_thumbnail_url(get_the_ID(), $imageSizes[$i]);
								}
							else:
								if( have_rows('project_gallery', get_the_ID()) ):
									$j = 0;

									while ( have_rows('project_gallery', get_the_ID()) ) : the_row();
										if ($j == 0) {
											$project_image = get_sub_field('project_gallery_image', get_the_ID());
											$project_image = $project_image['url'];
											$j++;
										}
									endwhile;
								endif;
							endif;


							// Build our array with the data we need using key => value
							$projects_data[] = [
								'title' => get_the_title(get_the_ID()),
								'sub_title'  => get_field('project_sub_title', get_the_ID()),
								'image_url' => $project_image,
								'post_link' => get_permalink(get_the_ID())
							];

							if ($blockHeight[$i] == 'half-row' ) {
								if ($blockHeight[$i] == 'half-row' && $blockHeight[$k] == 'half-row') {
									?>
									<div class="<?= $grid[$k]; ?>">
										<a href="<?= $projects_data[$k]['post_link']; ?>" class="project" data-aos="fade-up">
											<div class="content">
												<h4><?= $projects_data[$k]['title']; ?></h4>
												<h5><?= $projects_data[$k]['sub_title']; ?></h5>
											</div>
											<p class="view-project-link"><span class="append-arrow">View project</span></p>
											<div class="overlay"></div>
											<span class="lazy-loader"></span>
											<div class="image lazy <?= $blockHeight[$k]; ?> half-half" data-src="<?= $projects_data[$k]['image_url']; ?>"></div>
										</a>
										<a href="<?= $projects_data[$i]['post_link']; ?>" class="project" data-aos="fade-up">
											<div class="content">
												<h4><?= $projects_data[$i]['title']; ?></h4>
												<h5><?= $projects_data[$i]['sub_title']; ?></h5>
											</div>
											<p class="view-project-link"><span class="append-arrow">View project</span></p>
											<div class="overlay"></div>
											<span class="lazy-loader"></span>
											<div class="image lazy <?= $blockHeight[$i]; ?> half-half" data-src="<?= $projects_data[$i]['image_url']; ?>"></div>
										</a>
									</div>

									<?php
								}

							}  else {
								if ($i == 4) {
									?>

									<div class="col-12 col-md-6" data-aos="fade-up">
										<div class="news">
											<div class="half-post">
												<div class="row">
													<div class="col-12 col-md-6 order-sm-0 order-2">
														<a class="d-block" href="<?= $post_data[0]['post_link']; ?>">
															<div class="news-block bk-orange half">
																<h2><?= $post_data[0]['title']; ?></h2>
																<p class="read-more append-arrow">Read more</p>
															</div>
														</a>
													</div>
													<div class="col-12 col-md-6">
														<a class="d-block news-block-image-wrap half" href="<?= $post_data[0]['post_link']; ?>">
															<span class="lazy-loader"></span>
															<div class="news-block-image lazy" data-src="<?= $post_data[0]['image_url']; ?>"></div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php 
									if ( !empty($desktop_image['sizes']['grid-half-half']) ) {
										?>
										<div class="col-md-6" data-aos="fade-up" id="contact-details">
											<a href="<?= $link; ?>">	
												<picture>
													<source media="(min-width: 768px)" srcset="<?= $desktop_image['sizes']['grid-half-half']; ?>">
													<source media="(max-width: 767px)" srcset="<?= $mobile_image['sizes']['grid-half-half']; ?>">
													<img style="object-fit: cover; height: 100%; max-width: 100%; width: 100%;" src="<?= $desktop_image['sizes']['grid-half-half']; ?>" alt="<?= $desktop_image['alt']; ?>" />
												</picture>
											</a>
										</div>
										<?php
									} else {
									?>
										<div class="col-md-6 lazy" data-aos="fade-up" id="contact-details" data-src="<?= get_template_directory_uri();?>/assets/img/co-contact-details.jpg">
											<div class="center">
												<h2 class="text-orange">Contact</h2>
												<p class="h2"><span class="icon icon--phone"><?= get_field('globals_telephone_number', 'options'); ?></span></p>
												<p class="h2"><span class="icon icon--email"><?= get_field('globals_email_address', 'options'); ?></span></p>
											</div>
										</div>
									<?php
									}	
									?>
									<div class="<?= $grid[$i]; ?>" data-aos="fade-up">
										<a href="<?= $projects_data[$i]['post_link']; ?>" class="project">
											<div class="content">
												<h4><?= $projects_data[$i]['title']; ?></h4>
												<h5><?= $projects_data[$i]['sub_title']; ?></h5>
											</div>
											<p class="view-project-link"><span class="append-arrow">View project</span></p>
											<div class="overlay"></div>
											<span class="lazy-loader"></span>
											<div class="image lazy <?= $blockHeight[$i]; ?>" data-src="<?= $projects_data[$i]['image_url']; ?>"></div>
										</a>
									</div>
									<?php
								}
								elseif ($i == 7) {
									$awards_url =  get_permalink( get_page_by_title( 'Our awards' ) );
									?>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12" data-aos="fade-up">
												<div class="bk-light awards blog">
													<h3 class="text-orange">Awards</h3><a href="<?= $awards_url; ?>" class="append-arrow">Read more</a>
												</div>

											</div>
											<div class="col-md-12" data-aos="fade-up">
												<a href="<?= $projects_data[$i]['post_link']; ?>" class="project">
													<div class="content">
														<h4><?= $projects_data[$i]['title']; ?></h4>
														<h5><?= $projects_data[$i]['sub_title']; ?></h5>
													</div>
													<p class="view-project-link"><span class="append-arrow">View project</span></p>
													<div class="overlay"></div>
													<span class="lazy-loader"></span>
													<div class="image lazy half-row" data-src="<?= $projects_data[$i]['image_url']; ?>"></div>
												</a>
											</div>
										</div>
									</div>

									<?php /* ?>
									<div class="col-md-6 blogcontent" style="">
										<!-- <div class="image half-row" ></div> -->
										<div class="text-content bk-orange text-white">
											<h3 class="text-white"><?= $post_data[0]['title']; ?><br><?= $post_data[0]['post_date']; ?></h3>
											<a href="<?= $post_data[1]['post_link']; ?>" class="text-white append-arrow white-arrow">Read more</a>
										</div>
									</div> <?php */ ?>


									<div class="news col-12 col-md-6" data-aos="fade-up">
										<div class="row">
											<div class="col-12 full">
												<a href="<?= $post_data[1]['post_link']; ?>">
													<div class="news-block-seventy">
														<div class="news-block-image-wrap seventy">
															<span class="lazy-loader"></span>
															<div class="news-block-image lazy" data-src="<?= $post_data[1]['image_url']; ?>"></div>
														</div>
													</div>
													<div class="news-block-thirty">
														<div class="news-block bk-orange thirty">
															<h2><?= $post_data[1]['title']; ?></h2>
															<p class="read-more append-arrow">Read more</p>
														</div>
													</div>
												</a>
											</div>
										</div>
									</div>

									
									
									<?php		
								} else {
									?>
									<div class="<?= $grid[$i]; ?>" data-aos="fade-up">
										<a href="<?= $projects_data[$i]['post_link']; ?>" class="project">
											<div class="content">
												<h4><?= $projects_data[$i]['title']; ?></h4>
												<h5><?= $projects_data[$i]['sub_title']; ?></h5>
											</div>
											<p class="view-project-link"><span class="append-arrow">View project</span></p>
											<div class="overlay"></div>
											<span class="lazy-loader"></span>
											<div class="image lazy <?= $blockHeight[$i]; ?>" data-src="<?= $projects_data[$i]['image_url']; ?>"></div>
										</a>
									</div>
									<?php
								}
							}
							wp_reset_postdata();
							$i++;
						endif;
					endwhile;
				endif;
				?>
			</div>
		</div>
	</section>

	<?php get_section_project_start(); ?>

	<?php get_footer();