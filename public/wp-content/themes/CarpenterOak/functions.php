<?php // Functions File

// Set directory vars
$theme_url = get_stylesheet_directory_uri();
$theme_dir = get_stylesheet_directory();

// Auto activate plugin
function run_activate_plugin( $plugin ) {
	$current = get_option( 'active_plugins' );
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin = plugin_basename( trim( $plugin ) );

	if ( !in_array( $plugin, $current ) ) activate_plugin( $plugin );

	return null;
}

add_theme_support( 'post-thumbnails' );



// Activate required plugins
if ( !class_exists('dbHelper') ) run_activate_plugin( 'db-wp-core/db-wp-core.php' );

// Styles & Scripts
if ( !is_admin() ) {
	if ( dbHelper::is_env( 'live' ) ) {
		dbHelper::add_styles( [
			[ 'font', 'https://fonts.googleapis.com/css?family=Lato:400,900' ],
			[ 'lib-style', $theme_url . '/assets/css/dist/lib.min.css' ],
			[ 'core-style', $theme_url . '/assets/css/dist/style.min.css' ],
		] );
		dbHelper::add_scripts( [
			[ 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCmuUK2948CA0Zt-MJRgDIM-aO918D-QFg' ],
			[ 'lib-script', $theme_url . '/assets/js/dist/lib.min.js', 'jquery' ],
			[ 'core-script', $theme_url . '/assets/js/dist/core.min.js' ],
		] );
	} else {
		dbHelper::add_styles( [
			[ 'font', 'https://fonts.googleapis.com/css?family=Lato:400,900' ],
			[ 'lib-style', $theme_url . '/assets/css/src/lib.css' ],
			[ 'core-style', $theme_url . '/assets/css/src/style.css' ],
		] );
		dbHelper::add_scripts( [
			[ 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCmuUK2948CA0Zt-MJRgDIM-aO918D-QFg' ],
			[ 'lib-script', $theme_url . '/assets/js/dist/lib.min.js', 'jquery' ],
			[ 'main-script', $theme_url . '/assets/js/src/jquery/main.js' ],
			[ 'image-script', $theme_url . '/assets/js/src/image-scroll.js' ],
			[ 'acf-gmaps', $theme_url . '/assets/js/src/acf-gmap.js' ],
			[ 'project-start', $theme_url . '/assets/js/src/jquery/project-start.js' ],
		] );
	}
}
if ( dbHelper::is_env( 'dev' ) ) {
//	show_admin_bar(false);
}


// Includes
foreach ( glob( $theme_dir . '/includes/*.php' ) as $file ) {
	include_once( $file );
}

// Disable default linked embedded images
function wpb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );

	if ( $image_set !== 'none' ) {
		update_option( 'image_default_link_type', 'none' );
	}
}
add_action( 'admin_init', 'wpb_imagelink_setup', 10 );

add_theme_support( 'post-thumbnails' );


//remove contact form 7 default styling
function wps_deregister_styles() {
    wp_deregister_style( 'contact-form-7' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

// Display sub menu on pages
function the_sub_menu($show_menu) {
	global $post;
	global $menu_shown_already;

	$menu_shown_already = $show_menu;
	$smenu = $menu_shown_already . '<div class="collapse" id="sidebarMenu"><ul class="sub_menu">';

	if (!$post->post_parent) :

		$smenu .= '<li class="current_page_item"><a href="' . get_permalink($post->ID) . '">' . $post->post_title . '</a></li>';
	    $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
	    $smenu .= $children;
	    $ancestors = $post->ID;

	else :

	    if ($post->ancestors) :

	        $ancestors = end(get_post_ancestors( $post->ID ));
	        $smenu .= '<li><a href="' . get_permalink($ancestors) . '">' . get_the_title($ancestors) . '</a></li>';
	        $children = wp_list_pages("title_li=&child_of=".$ancestors."&echo=0");
	        $smenu .= $children;

	    endif; 

	endif;

	$smenu .= '</ul></div>';

	if ($menu_shown_already == false ) :

		$menu_shown_already = true;
		return $smenu;

	endif;

}

function sidebar_menu_setup() {
	$show_sub_menu = get_field('show_sub_menu_sidebar', get_the_ID()); 
	global $menu_shown_already;

	if (!$menu_shown_already) :

		$sidebar_filter = '<div class="side-bar__filter d-lg-none"><a class="side-bar__filter--link" data-toggle="collapse" data-target="#sidebarMenu">sub-menu <div class="nav-icon">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div></a></div>';

	else :

		$sidebar_filter = '';

	endif;

	$show_sub_menu_classes = ($show_sub_menu == 'yes') ? $sidebar_filter . '<div class="col-md-12 col-lg-3 sub-menu side-bar">' . the_sub_menu($menu_shown_already) . '</div>' : '';

	$show_sub_menu_content_classes = ($show_sub_menu == 'yes') ? '<div class="col-md-12 col-lg-9 sub-content">' : '<div class="col sub-content">';
	echo $show_sub_menu_classes;
	echo $show_sub_menu_content_classes;
	$menu_setup = $show_sub_menu_classes;
	$menu_setup .= $show_sub_menu_content_classes;
	return $menu_setup;

}

function container_width() {
	$container_width = get_field('full_width_container', get_the_ID() );

	if (!isset($container_width)){
		$container = 'container-fluid';

	} else {
		if ($container_width == 'yes' ) {
			$container = 'container-fluid';
		} else {
			$container = 'container';
		}
	}


	
	return $container;
}

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyCmuUK2948CA0Zt-MJRgDIM-aO918D-QFg');
}

add_action('acf/init', 'my_acf_init');



function modify_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Enter your search"/>
    </form>';
 
    return $form;
}
add_filter( 'get_search_form', 'modify_search_form' );


/**
 * Limit search results to certain CPT
 */
function searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('post','page','team','project'));
    }
 
return $query;
}
 
add_filter('pre_get_posts','searchfilter');



/**
 * Make search index custom fields
 */
/**
 * Extend WordPress search to include custom fields
 * http://adambalee.com
 *
 * Join posts and postmeta tables
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;
    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }
    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;
    if ( is_search() ) {
        return "DISTINCT";
    }
    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );


//remove paging for 'projects'
// function no_nopaging($query) {
// 	if ('project' == get_post_type()) {
// 		$query->set('nopaging', 1);
// 	}
// }

// add_action('parse_query', 'no_nopaging');


function remove_textarea() {
	remove_post_type_support( 'page', 'editor' );
}
add_action('admin_init', 'remove_textarea');


//remove 'building type' and 'category' from archive titles
function modify_archive_title( $title ) { 

	$title = str_replace(array("Building Type: ", "Category: "), "", $title);

	return $title;

}

add_filter( 'get_the_archive_title', 'modify_archive_title' ); 

// allow php to show in the JS debug console in chrome
function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}


function get_building_type_id(){

	//array for holding assocaited project categories against 'building-type'
	$building_type = array();

	// get term id and name from 'building-type' taxonomy
	$term_id =  get_queried_object()->term_id;
	$term = get_term( $term_id );
	$term_taxonomy = $term->taxonomy;

	//get posts from queried term (either 'residentual' or 'commericial')
	$args = array(
		'post_type' => 'project',
		'posts_per_page' => '-1',
		'tax_query'  => array(
			array(
				'taxonomy' => $term_taxonomy,
				'field'    => 'term_id',
				'terms'    => array($term_id)
			),
		),
	);

	$query = new WP_Query($args);

	if ( $query->have_posts() ) : 
		while ( $query->have_posts() ) : $query->the_post(); 

			$post_id = get_the_ID();

			// get catoegies associated with a particular post
			$post_building_type = wp_get_post_terms($post_id, 'building-type', array( 'fields' => 'all' ));

			// store project categories of above post into array for sorting later
			foreach($post_building_type as $k) :

				$building_type[] = array(
					'term_id' => $k->term_id,
					'term_name'  => $k->name,
					'term_slug'  => $k->slug,
				);

			endforeach;
		endwhile;
	endif;

	// remove duplicates from $building_type array
	$ordered_building_type = array_unique($building_type, SORT_REGULAR);

	// sort array alphabetically by term name
	usort($ordered_building_type, function($a, $b) {
		return $a['term_name'] <=> $b['term_name'];
	});


	foreach($ordered_building_type as $b) :

		$building_type_list .= $b['term_id'];

	endforeach;

	return (int)$building_type_list;

}

function get_locations_id(){

	//array for holding assocaited project categories against 'locations'
	$locations = array();

	// get term id and name from 'locations' taxonomy
	$term_id =  get_queried_object()->term_id;
	$term = get_term( $term_id );
	$term_taxonomy = $term->taxonomy;

	//get posts from queried term (either 'residentual' or 'commericial')
	$args = array(
		'post_type' => 'project',
		'posts_per_page' => '-1',
		'tax_query'  => array(
			array(
				'taxonomy' => $term_taxonomy,
				'field'    => 'term_id',
				'terms'    => array($term_id)
			),
		),
	);

	$query = new WP_Query($args);

	if ( $query->have_posts() ) : 
		while ( $query->have_posts() ) : $query->the_post(); 

			$post_id = get_the_ID();

			// get catoegies associated with a particular post
			$post_locations = wp_get_post_terms($post_id, 'locations', array( 'fields' => 'all' ));

			// store project categories of above post into array for sorting later
			foreach($post_locations as $k) :

				$locations[] = array(
					'term_id' => $k->term_id,
					'term_name'  => $k->name,
					'term_slug'  => $k->slug,
				);

			endforeach;
		endwhile;
	endif;

	// remove duplicates from $locations array
	$ordered_locations = array_unique($locations, SORT_REGULAR);

	// sort array alphabetically by term name
	usort($ordered_locations, function($a, $b) {
		return $a['term_name'] <=> $b['term_name'];
	});


	foreach($ordered_locations as $b) :

		$locations_list .= $b['term_id'];

	endforeach;

	return $locations_list;

}


function show_project_filters($list, $ajax){

	//array for holding assocaited project categories against 'building-type'
	$project_categories = [];

	// get term id and name from 'building-type' taxonomy
	$term_id =  get_queried_object()->term_id;
	$term = get_term( $term_id );
	$term_taxonomy = $term->taxonomy;
	$term_name = $term->name;

	//get posts from queried term (either 'residentual' or 'commericial')
	$args = [
		'post_type' => 'project',
		'posts_per_page' => '-1',
		'tax_query'  => [
			[
				'taxonomy' => $term_taxonomy,
				'field'    => 'term_id',
				'terms'    => [ $term_id ]
			],
		],
	];

	$query = new WP_Query($args);

	if ( $query->have_posts() ) : 
		while ( $query->have_posts() ) : $query->the_post(); 

			$post_id = get_the_ID();

			// get catoegies associated with a particular post
			$post_project_categories = wp_get_post_terms($post_id, $list, [ 'fields' => 'all' ] );

			// store project categories of above post into array for sorting later
			foreach($post_project_categories as $k) :

				$term = get_term($k->term_id, 'project-category');

				$project_categories[] = [
					'term_id' => $k->term_id,
					'term_name'  => $k->name,
					'term_taxonomy'  => $k->taxonomy,
					'term_slug'  => $k->slug,
					'term_count' => $term->count
				];

			endforeach;
		endwhile;
	endif;

	// remove duplicates from $project_categories array
	$ordered_project_cateogories = array_unique($project_categories, SORT_REGULAR);

	// sort array alphabetically by term name
	usort($ordered_project_cateogories, function($a, $b) {
		return $a['term_name'] <=> $b['term_name'];
	});

	$project_cateogories_list = ( $ajax == true ) ? '<ul id="menu" class="sub project-sub">' : '<ul>';


	foreach($ordered_project_cateogories as $b) :
		$active = ( $_GET['project-category'] == strtolower($b['term_slug']) ) ? 'active ' : '';
	endforeach;
	
	if ( empty($_GET['project-category']) ) {
		$active = 'active';
	}
	
	$locationPage = is_tax( 'locations', get_locations_id() );
	
	// add 'all' to menu
	$project_cateogories_list .= ( $ajax == true ) ? '<li class="d-none d-lg-inline-block">
													<input class="ajax-filter hide-radio '. $active .'" id="filter_all" type="radio" data-template="archive/items" data-category="" data-termid="' . get_building_type_id() . '" name="filter[project-category][]" value="*"  /> 
													<label class="menu-item  ' . $active .'" for="filter_all">All</label>
													  </li>' : '';

	$i = 0;
	foreach($ordered_project_cateogories as $b) :
	$i ++;


		

		// check to see if menu item should be omitted from nav and filters
		if (!get_field('archive_menu_omit', $b['term_taxonomy'] . '_' . $b['term_id'])) :

			if ($ajax == true) :

				$hyphenTerm = str_replace(' ', '-', $b['term_slug']);
				$lowercaseTerm = strtolower($hyphenTerm);

				$active = ( $_GET['project-category'] == $lowercaseTerm ) ? 'active ' : '';

				$project_cateogories_list .= '<li>
												<input class="ajax-filter hide-radio '. $active .'" id="filter_' . $b['term_taxonomy'] . '_' . $b['term_id'] . '" type="radio" data-template="archive/items" data-category="?project-category=' . $lowercaseTerm . '" data-termid="' . $b['term_id'] . '" name="filter[' . $b['term_taxonomy'] . '][]" value="' . $b['term_id'] . '" /> 
												<label class="menu-item ' . $active . ' " for="filter_' . $b['term_taxonomy'] . '_' . $b['term_id'] . '">' .$b['term_name'] . '</label>
											 </li>';

			else :
				 
				if ( $i == 7 ) {
					$project_cateogories_list .= '</ul><ul>';
				}

				$project_cateogories_list .= ' <li>
												<input class="styled-checkbox" id="filter_' . $b['term_taxonomy'] . '_' . $b['term_id'] . '" type="checkbox" data-template="archive/items" name="filter[' . $b['term_taxonomy'] . '][]" value="' . $b['term_id'] . '" /> 
												<label for="filter_' . $b['term_taxonomy'] . '_' . $b['term_id'] . '">' .$b['term_name'] . '</label>
											  </li>';

			endif;

		endif;

	endforeach;

	if($locationPage) {
		$project_cateogories_list .= ( $ajax == true ) ? '<li class="d-none">
									<input id="filter_hidden" type="hidden" data-template="archive/items" data-category="?" data-termid="' . get_locations_id() . '" name="filter[locations][]" value="'  . get_locations_id() . '" checked  /> 
								  </li>' : '';
		
	} else {
		$project_cateogories_list .= ( $ajax == true ) ? '<li class="d-none">
									<input id="filter_hidden" type="hidden" data-template="archive/items" data-category="?" data-termid="' . get_building_type_id() . '" name="filter[building-type][]" value="'  . get_building_type_id() . '" checked  /> 
								  </li>' : '';
	}
	

	

	// add 'more filters' to end of menu list
	$project_cateogories_list .= ( $ajax == true ) ? '<li class="more-filters"><a class="filters" data-toggle="collapse" href="#filters" role="button" aria-expanded="false" aria-controls="filters"><span class="d-none d-lg-inline">More </span>filters 
	<div class="nav-icon">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div></a></li></ul>' : '</ul>';

	return $project_cateogories_list;

}

function bybe_remove_yoast_json($data){
	$data = array();
	return $data;
}
add_filter('wpseo_json_ld_output', 'bybe_remove_yoast_json', 10, 1);

function num_posts() {
	global $wp_query;
	return $wp_query->post_count;
}


function number_of_posts_on_archive( $query ) {
	if ( $query->is_archive() && $query->is_main_query() && !is_admin() ) {
		$query->set( 'posts_per_page', 100 );
	}
}
add_action( 'pre_get_posts', 'number_of_posts_on_archive' );

add_action('parse_query', 'pmg_ex_sort_posts');
function pmg_ex_sort_posts($q)
{
	$q->set('orderby', ['menu_order' => 'ASC', 'date' => 'DESC']);
}

function wpse_75604_check_rewrite_conflicts( $qv ) {
	if ( isset( $qv['attachment'] ) ) {
		if ( ! get_page_by_path( $qv['attachment'], OBJECT, 'post' ) ) {
			$qv['name'] = $qv['attachment'];
			unset( $qv['attachment'] );
		}
	}
	return $qv;
}
add_filter( 'request', 'wpse_75604_check_rewrite_conflicts' );