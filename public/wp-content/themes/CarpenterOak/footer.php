		</div> <?php /* closing page tag */ ?>
		<?php
		

		
		$newsletterPopup = get_page_by_title('Newsletter', 'ARRAY_A', 'popupbuilder');
		if ($newsletterPopup && !is_front_page() ): ?>
			<div class="container-fluid">
				<div class="row">
					<div class="mx-auto text-center d-block d-md-none newsletter">
						Discover us with <a class="append-arrow sg-show-popup sgpb-popup-id-<?= $newsletterPopup['ID']?>" data-sgpbpopupid="<?= $newsletterPopup['ID']?>" data-popup-event="click">the Carpenter Oak newsletter</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<section class="container-fluid contact-cta">
			<div class="row">
				<div class="col-md-6 newsletter-block" style="background-image: url(<?= get_template_directory_uri();?>/assets/img/co-contact-details-orange.jpg)">
					<div class="wrap">
						<div>
							<span class="colour icon__social icon__social--logo text-orange">Carpenter Oak</span>
							<?php
							$socialChannels = get_field('social_media_channels', 'options');
							foreach ($socialChannels as $socialChannel) {
								?>
								<a href="<?= $socialChannel['Social_Media_Channel_URL']; ?>" target="_blank" class="icon__social icon__social--<?= $socialChannel['social_media_channel']; ?>"><?= $socialChannel['social_media_channel']; ?></a>
								<?php
							}
							?>
						</div>
						<div class="newsletter">
							<span>Discover us </span><a class="append-arrow white-arrow sg-show-popup sgpb-popup-id-<?= $newsletterPopup['ID']?>" data-sgpbpopupid="<?= $newsletterPopup['ID']?>" data-popup-event="click">Get the Carpenter Oak newsletter</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 contact-block">
					<div class="wrap">
						<div class="w-100">
							<a href="/contact/" class="btn append-arrow white-arrow">Start Your Project</a>
						</div>
						<div class="w-100">
							<?php $brochurePopup = get_page_by_title('Brochure', 'ARRAY_A', 'popupbuilder'); ?>
							<div class="btn append-arrow white-arrow sg-show-popup sgpb-popup-id-<?= $brochurePopup['ID']?>" data-sgpbpopupid="<?= $brochurePopup['ID']?>" data-popup-event="click">Download Brochure</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<footer id="footer" class="bg-light d-none d-md-block" style="background-image: url(<?= get_template_directory_uri();?>/assets/img/co-wooden-texture.jpg)">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-lg-3">
						<h2>About us</h2>
						<?php wp_nav_menu( array( 'theme_location' => 'footer_about', 'menu_id' => 'menu', 'container' => 'nav', 'menu_class'=> 'footer' ) ); ?>
					</div>
					<div class="col-md-6 col-lg-3">
						<h2>Project Gallery</h2>
						<?php wp_nav_menu( array( 'theme_location' => 'footer_projects', 'menu_id' => 'menu', 'container' => 'nav', 'menu_class'=> 'footer' ) ); ?>
					</div>
					<div class="col-md-6 col-lg-3">
						<h2>Our Expertise</h2>
						<?php wp_nav_menu( array( 'theme_location' => 'footer_experts', 'menu_id' => 'menu', 'container' => 'nav', 'menu_class'=> 'footer' ) ); ?>
					</div>
					<div class="col-md-6  col-lg-3">
						<h2>Our Locations</h2>
						<?php wp_nav_menu( array( 'theme_location' => 'footer_locations', 'menu_id' => 'menu', 'container' => 'nav', 'menu_class'=> 'footer' ) ); ?>
						<?php $ContactPopup = get_page_by_title('Contact', 'ARRAY_A', 'popupbuilder'); ?>
						<div class="btn btn-primary append-arrow white-arrow text-white footer-contact-btn sg-show-popup sgpb-popup-id-<?= $ContactPopup['ID']?>" data-sgpbpopupid="<?= $ContactPopup['ID']?>" data-popup-event="click">Contact Us</div>	
					</div>
				</div>
			</div>
		</footer>
		<div class="footer-bottom footer-details-contact">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-12 col-md-8">
						<div class="footer-details">
							<p class="small text-center text-md-left"><span>&copy; <?php echo date("Y"); ?> <?= get_field('globals_copyright', 'options');?> </span><?= get_field('globals_footer_address', 'options'); ?> </p>
						</div>
					</div>
					<div class="col-12 col-md-4 text-center text-md-right">
						<span id="back-to-top">Back to top</span>
					</div>
				</div>	
			</div>
		</div>
		<div class="footer-bottom footer-details-vat">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<?php if ( get_field('globals_footer_address_second', 'options')) {
							?>
								<p class="small text-center" id="footer-address-second"><?php echo get_field('globals_footer_address_second', 'options'); ?> </p>
							<?php
						} ?>
					</div>
				</div>
			</div>
		</div>
		


		<script>window.ajaxurl || (window.ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>');</script>
		<?php wp_footer(); ?>
	</body>
</html>
