<?php get_header();
the_post();
$page_id = get_the_ID(); ?>

	<section id="imageCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">

			<?php
			if (have_rows('project_gallery')):
				$count = count(get_field('project_gallery'));
				$i = 0;
				// loop through the rows of data
				while (have_rows('project_gallery')) : the_row();

					$image        = get_sub_field('project_gallery_image');
					$active_class = ($i == 0) ? ' active' : '';

					if ($i == 0) {
						echo '<div class="carousel-item'.$active_class.'" data-interval="5000"><div style="background-image: url('.$image['url'].')"></div></div>';
					} elseif ($i == ( $count - 1 ) ) {
						echo '<div class="carousel-item last'.$active_class.'" data-interval="5000"><div style="background-image: url('.$image['url'].')"></div></div>';
					} else {
						echo '<div class="carousel-item'.$active_class.'" data-interval="5000"><div style="background-image: url('.$image['url'].')"></div></div>';
					}

					$i++;
				endwhile;

			else :
				// no rows found
			endif;
			?>
		</div>
		<?php if ($count >= 2) : ?>
			<a class="carousel-control-prev" href="#imageCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#imageCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		<?php endif; ?>
	</section>
	<section id="sub-menu" class="bg-light project-section-link">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<nav>
						<ul id="menu" class="sub">
							<li>
								<a href="#overview" class="active link-to-scroll">Overview</a>
							</li>
							<li>
								<a href="#imageCarousel" class="link-to-scroll">Gallery</a>
							</li>
							<li>
								<a class="link-to-scroll" href="#content">What we did</a>
							</li>
							<?php if ( get_field('project_testimonials') ) {
								echo '<li> <a class="link-to-scroll" href="#testimonial">Testimonial</a> </li>';
							} ?>
							<?php if( get_field('project_team_members') ) {
								echo '<li> <a class="link-to-scroll" href="#meetTheTeam">Meet the team</a> </li>';
							} ?>


						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<?php
		$votingProjectsID = get_field('voting_projects', 'options');
		$votingProjects = [];
		$currentProject = get_the_title();

		$buildingType = get_field('project_building_tpye');
	
		if ($buildingType->slug === 'residential-projects'){
			$project_vote = 'residential-vote';
		} else if ($buildingType->slug === 'commercial-projects') {
			$project_vote = 'commercial-vote';
		}
		

		foreach ($votingProjectsID as $votingProjectID) {
			$votingProjects[] = get_the_title($votingProjectID);
		}

		if (in_array($currentProject, $votingProjects)) {
			?>
			<section class="vote-banner">
				<p>Vote for this project now in the 32 years of Carpenter Oak vote</p>
				<form action="/32-years-of-carpenter-oak/" method="post">
					<input type="hidden" name="<?= $project_vote; ?>" value="<?= $currentProject; ?>">
					<button value="submit" class="btn btn-primary append-arrow white-arrow text-white">Vote</button>
				</form>
			</section>
			<?php
		}
	?>
	<section id="overview">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">
					<h1 class="text-orange"><?php echo get_the_title(); ?></h1>
					<h3><?php echo get_field('project_sub_title'); ?></h3>
					<div class="project__specifics">
						<?php

							if (get_field('project_client')) :

								?>

									<p><span class="project__specifics--title">Client:</span> <span class="project__specifics--detail"><?php echo get_field('project_client'); ?></span></p>

								<?php

							endif;

							if (get_field('project_location')) :

								?>

									<p><span class="project__specifics--title">Location:</span> <span class="project__specifics--detail"><?php echo get_field('project_location'); ?></span></p>

								<?php

							endif;

							if (get_field('project_completed')) :

								?>

									<p><span class="project__specifics--title">Completed:</span> <span class="project__specifics--detail"><?php echo get_field('project_completed'); ?></span></p>

								<?php

							endif;


							if( have_rows('project_further_specifics') ):

							 	// loop through the rows of data
							    while ( have_rows('project_further_specifics') ) : the_row();

							        echo '<p><span class="project__specifics--title">' . get_sub_field('project_further_specifics_title') . ':</span> <span class="project__specifics--detail">' . get_sub_field('project_further_specifics_text') . '</span></p>';

							    endwhile;

							endif;

						?>
					</div>
				</div>
				<div class="col-md-8">
					<h2 class="text-orange"><?php echo get_field('project_title'); ?></h2>
					<?php echo get_field('project_short_overview'); ?>
					<a class="append-arrow link-to-scroll" href="#content">
						Read more
					</a>
				</div>
			</div>
		</div>
	</section>

	<section id="imageBlockCarousel">
		<div class="container-fluid">
			<div class="row">
				<?php
				$rows = array();
					if( have_rows('1st_image_block') ):

						$count = count(get_field('1st_image_block'));

						if ($count == 1) {
							$grid        = array('col-md-12');
							$imageHeight = array('full-col');
						}

						elseif ($count == 2) {
							$grid        = array('col-md-6', 'col-md-6');
							$imageHeight = array('full-col', 'full-col');
						}

						elseif ($count == 3) {

							if (get_field('1st_image_block_layout') == '1left_2right') :

								$grid        = array('col-md-6', 'col-md-6', 'col-md-6');
								$imageHeight = array('full-col', 'half-row', 'half-row');

							elseif (get_field('1st_image_block_layout') == '2left_1right') :

								$grid        = array('col-md-6', 'col-md-6', 'col-md-6');
								$imageHeight = array('half-row', 'half-row', 'full-col');

							elseif (get_field('1st_image_block_layout') == '1top_2bottom') :

								$grid        = array('col-md-12', 'col-md-6', 'col-md-6');
								$imageHeight = array('full-col', 'half-row', 'half-row');

							elseif (get_field('1st_image_block_layout') == '2top_1bottom') :

								$grid        = array('col-md-6', 'col-md-6', 'col-md-12');
								$imageHeight = array('half-row', 'half-row', 'full-col');

							else :

								$grid        = array('col-md-12', 'col-md-6', 'col-md-6');
								$imageHeight = array('full-col', 'full-col', 'full-col');

							endif;

						}

						else {
							$grid        = array('col-md-12', 'col-md-6', 'col-md-6');
							$imageHeight = array('full-col', 'half-row', 'half-row');
						}

						$i = 0;

						if ($count == 3) :

							if (get_field('1st_image_block_layout') == '1left_2right') :

								// loop through the rows of data
								while ( have_rows('1st_image_block') ) : the_row();
									$rows[] =  get_sub_field('1st_image_block_image');

									if ($i == 0) :
										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div>
										</div>';

									elseif ($i == 1 ) :
										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div>';
									elseif ($i == 2) :
										echo '<span class="lazy-loader"></span><div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div></div>';
									endif;

									$i++;
								endwhile;

							elseif (get_field('1st_image_block_layout') == '2left_1right') :

						 	// loop through the rows of data
								while ( have_rows('1st_image_block') ) : the_row();
									$rows[] =  get_sub_field('1st_image_block_image');

									if ($i == 0 ) :
										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-half'] . '"></div>';
									elseif ($i == 1) :
										echo '<span class="lazy-loader"></span><div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-half'] . '"></div></div>';
									elseif ($i == 2) :

										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-full'] . '"></div>
										</div>';
									endif;

									$i++;
								endwhile;

							elseif (get_field('1st_image_block_layout') == '1top_2bottom' || get_field('1st_image_block_layout') == '2top_1bottom') :

						 	// loop through the rows of data
								while ( have_rows('1st_image_block') ) : the_row();
									$rows[] =  get_sub_field('1st_image_block_image');

									echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
									<span class="lazy-loader"></span>
									<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div></div>';

									$i++;
								endwhile;

							endif;

						else :

							while ( have_rows('1st_image_block') ) : the_row();
									$rows[] =  get_sub_field('1st_image_block_image');

									echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
									<span class="lazy-loader"></span>
									<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-full'] . '"></div></div>';

									$i++;
								endwhile;

						endif;

					endif;

				?>
			</div>
		</div>
	</section>

	<?php

	$posts_overview = get_field('project_full_overview');

	if( $posts_overview ): ?>

	<section class="sidebar-no-menu">
		<div class="container-fluid">
			<div class="row">
				<div class="col-3 bk-grey d-none d-lg-block">
				</div>
				<div class="col-12 col-lg-9">
					<div id="content">
						<?php echo $posts_overview; ?>
					</div>
				</div>
			</div>

		</div>
	</section>

	<?php endif; ?>

	<?php

	get_section_team();

	?>

			<section id="imageBlockCarousel">
		<div class="container-fluid">
			<div class="row">
				<?php
				$rows = array();
					if( have_rows('2nd_image_block') ):

						$count = count(get_field('2nd_image_block'));

						if ($count == 1) {
							$grid = array('col-md-12');
							$imageHeight = array('full-col');
						}

						elseif ($count == 2) {
							$grid = array('col-md-6', 'col-md-6');
							$imageHeight = array('full-col', 'full-col');
						}

						elseif ($count == 3) {

							if (get_field('2nd_image_block_layout') == '2left_1right') :

								$grid = array('col-md-6', 'col-md-6', 'col-md-6');
								$imageHeight = array('half-row', 'half-row', 'full-col');

							elseif (get_field('2nd_image_block_layout') == '1left_2right') :

								$grid = array('col-md-6', 'col-md-6', 'col-md-6');
								$imageHeight = array ('full-col', 'half-row', 'half-row');

							elseif (get_field('2nd_image_block_layout') == '1top_2bottom') :

								$grid = array('col-md-12', 'col-md-6', 'col-md-6');
								$imageHeight = array('full-col', 'half-row', 'half-row');

							elseif (get_field('2nd_image_block_layout') == '2top_1bottom') :

								$grid = array('col-md-6', 'col-md-6', 'col-md-12');
								$imageHeight = array('half-row', 'half-row', 'full-col');

							else :

								$grid = array('col-md-12', 'col-md-6', 'col-md-6');
								$imageHeight = array('full-col', 'full-col', 'full-col');

							endif;

						}

						else {
							$grid = array('col-md-12', 'col-md-6', 'col-md-6');
							$imageHeight = array('full-col', 'half-row', 'half-row');
						}

						$i = 0;

						if ($count == 3) :

							if (get_field('2nd_image_block_layout') == '1left_2right' ) :

						 	// loop through the rows of data
								while ( have_rows('2nd_image_block') ) : the_row();
									$rows[] =  get_sub_field('2nd_image_block_image');

									if ($i == 0) :
										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div>
										</div>';

									elseif ($i == 1 ) :
										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div>';
									elseif ($i == 2) :
										echo '<span class="lazy-loader"></span><div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div></div>';
									endif;

									$i++;
								endwhile;

							elseif (get_field('2nd_image_block_layout') == '2left_1right') :


						 	// loop through the rows of data
								while ( have_rows('2nd_image_block') ) : the_row();
									$rows[] =  get_sub_field('2nd_image_block_image');

									if ($i == 0 ) :
										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-half'] . '"></div>';
									elseif ($i == 1) :
										echo '<span class="lazy-loader"></span><div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-half'] . '"></div></div>';
									elseif ($i == 2) :

										echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
										<span class="lazy-loader"></span>
										<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['sizes']['grid-half-full'] . '"></div>
										</div>';
									endif;

									$i++;
								endwhile;


							elseif (get_field('2nd_image_block_layout') == '1top_2bottom' || get_field('2nd_image_block_layout') == '2top_1bottom') :

						 	// loop through the rows of data
								while ( have_rows('2nd_image_block') ) : the_row();
									$rows[] =  get_sub_field('2nd_image_block_image');

									echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
									<span class="lazy-loader"></span>
									<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div></div>';

									$i++;
								endwhile;

							endif;

						else :



							while ( have_rows('2nd_image_block') ) : the_row();
									$rows[] =  get_sub_field('2nd_image_block_image');

									echo '<div class="' . $grid[$i] . '" data-aos="fade-up">
									<span class="lazy-loader"></span>
									<div class="image lazy ' . $imageHeight[$i] . '" data-src="' . $rows[$i]['url'] . '"></div></div>';

									$i++;
								endwhile;

						endif;

					endif;

				?>
			</div>
		</div>
	</section>

	<?php get_section_testimonial(); ?>


	<?php get_section_related_projects(); ?>

	<?php get_section_cta(); ?>




<?php get_footer(); ?>
