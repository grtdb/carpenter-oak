<?php

get_header();
$term = get_queried_object();



if ( have_posts() ) :
	$taxonomy = get_queried_object();
	?>
	<section id="archive--intro">
		<div class="container-fluid bk-light">
			<div class="row">
				<div class="col archive--intro__content">
					<?php
					$activeTerm = $_GET['project-category'];

					if (!empty($activeTerm)) {
					
					$termbyname = get_term_by('slug', $activeTerm, 'project-category');
					?>
					<h1 class="page-title text-orange"><?= $termbyname->name; ?></h1>
					<div class="archive-description">
						<?= $termbyname->description; ?>
					</div>
					<?php
					} else {
						echo '<h1 class="page-title text-orange">' . single_cat_title( '', false ) . '</h1>';
						the_archive_description( '<div class="archive-description">', '</div>' );
					}
					?>

				</div>
			</div>
		</div>
	</section>
	<section id="sub-menu" class="bg-white">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
				<nav>
					<form class="menuFilters">
						<?= show_project_filters('project-category', true); ?>
					</form>
				</nav>
				</div>
			</div>
		</div>
	</section>
	<section class="collapse" id="filters">
		<div class="container-fluid">
			<form class="filters">
				<div class="row">
					<div class="col-lg-4 content">
						<h3>Locations:</h3>
						<?= show_project_filters('locations', false); ?>
					</div>
					<div class="col-lg-4 content">
						<h3>Architect:</h3>
						<?= show_project_filters('architects', false); ?>
					</div>
					<div class="col-lg-4 content last">
						<h3>Style:</h3>
						<?= show_project_filters('styles', false); ?>
					</div>
					<div class="filters__close-filters" data-toggle="collapse" data-target="#filters">
						<div class="btn btn-lg btn-secondary"></div>
						<span>Filters</span>
					</div>
				</div>
				<div class="filter-buttons">
					<div class="btn btn-secondary filters__clear-filters">Clear Filters</div>
					<div class="btn btn-secondary filters__apply-filters" data-toggle="collapse" data-target="#filters">Apply Filters</div>
				</div>
			</form>
		</div>
	</section>
	<section id="projects">
		<div class="container-fluid no-pad">
			<div class="ajax-wrapper row">
				<?php dbHelper::get_part( 'archive/items' ); ?>
			</div>
			<?php /* dbHelper::get_part( 'loadmore', [ 'template' => 'archive/items' ] ); */?>
		</div>
	</section>
<?php else:
	get_template_part( 'template-parts/content', 'none' );

endif; ?>

<?php
get_footer();