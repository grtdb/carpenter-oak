<!DOCTYPE html>
<html>
	<head>
		<?php 
		if (strpos($_SERVER['REQUEST_URI'], "?project-category") !== false){
			$url = 'https://' . $_SERVER['SERVER_NAME'] .'/project-category/' . $_GET['project-category'];
			add_filter( 'wpseo_canonical', '__return_false',  10, 1 );
			?>
			<link id="canonical-project" rel="canonical" href="<?= $url; ?>/" />
			<?php
		}
		?>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php wp_title( ' | ', true, 'right' ); ?></title>
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?= get_template_directory_uri();?>/favicon.ico" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?= get_template_directory_uri(); ?>/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?= get_template_directory_uri(); ?>/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?= get_template_directory_uri(); ?>/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?= get_template_directory_uri(); ?>/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?= get_template_directory_uri(); ?>/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri(); ?>/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?= get_template_directory_uri(); ?>/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri(); ?>/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri(); ?>/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?= get_template_directory_uri(); ?>/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri(); ?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?= get_template_directory_uri(); ?>/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri(); ?>/favicon-16x16.png">
		<link rel="manifest" href="<?= get_template_directory_uri(); ?>/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?= get_template_directory_uri(); ?>/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!--[if lte IE 8]>
			<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?php //echo get_stylesheet_directory_uri(); ?>/assets/js/respond.min.js"></script>
		<![endif]-->
		<?php dbHelper::get_header_scripts(); ?>

		<script type='application/ld+json'>
			{
			  "@context": "http://www.schema.org",
			  "@type": "HomeAndConstructionBusiness",
			  "name": "Carpenter Oak",
			  "url": "https://carpenteroak.com/",
			  "logo": "https://carpenteroak.com/wp-content/uploads/2018/10/logo.jpg",
			  "description": "For over 30 years, we have been consistently redefining what it means to build with timber. Our confidence lies in our experience and unmatched creativity in designing and crafting purposeful and beautiful buildings.",
			  "address": {
			    "@type": "PostalAddress",
			    "streetAddress": "Cornworthy",
			    "addressLocality": "Totnes",
			    "addressRegion": "Devon",
			    "postalCode": "TQ9 7HF",
			    "addressCountry": "United Kingdom"
			  },
			  "openingHours": "Mo, Tu, We, Th, Fr 08:00-17:00",
			  "contactPoint": {
			    "@type": "ContactPoint",
			    "telephone": "01803732900",
			    "contactType": "general"
			  },
			  "sameAs" : [
			    "https://www.facebook.com/carpenteroakltd",
			    "https://www.instagram.com/carpenter_oak/",
			    "https://www.linkedin.com/company/carpenter-oak-ltd/",
			    "https://www.youtube.com/user/CarpenterOakltddevon",
			    "https://www.pinterest.co.uk/carpenteroak/",
			    "https://twitter.com/Carpenteroak" ]
			}
		 </script>
		<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5bf6d6629b95fc00123f97ed&product=custom-share-buttons"></script>
	</head>
	<body <?php body_class(); ?>>
		<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
		<!--[if lte IE 9]>
			<div class="es-browser-upgrade"><p>You are viewing this website on an outdated browser which may be insecure and may hinder your experience and limit functionality. Please consider upgrading. <a href="http://browsehappy.com" target="_blank">Click here to update</a></p></div>
		<![endif]-->
		<noscript>
			<style>.member{opacity: 1!important;}</style>
		</noscript>


		<header id="header" class="header <?=  ( is_front_page() ) ? 'home-header' : ''; ?>">
			<div class="container-fluid">
				<?php dbHelper::get_part( 'nav-button' ); ?>
				<div class="row">
					<div class="col-9 col-sm-10 col-md-4 col-lg-3">
						<a id="logo" href="/">
							<!-- <img class="img-fluid" src="<?= get_template_directory_uri();?>/assets/img/logo.png" alt="logo"> -->
						</a>
						
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-lg-9 main-nav">
						<div id="nav-wrap">
						<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_id' => 'menu', 'container' => 'nav', 'menu_class'=> 'header nav navbar-nav navbar-right' ) ); ?>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-8 col-lg-9 search-bar d-none">
						<div id="nav-wrap">
						<?php get_search_form(); ?>
						</div>
					</div>
				</div>
				</div>
			</div>
		</header>
		<div class="page-wrapper">